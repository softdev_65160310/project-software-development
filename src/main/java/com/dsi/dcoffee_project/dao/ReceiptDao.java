/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.dao;

import com.dsi.dcoffee_project.helper.DatabaseHelper;
import com.dsi.dcoffee_project.model.Receipt;
import com.dsi.dcoffee_project.model.Salary;
import com.dsi.dcoffee_project.model.WorkTime;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author BoM
 */
public class ReceiptDao implements Dao<Receipt>{

    @Override
    public Receipt get(int id) {
        Receipt receipt = null;
        String sql = "SELECT * FROM RECEIPT WHERE R_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                receipt = Receipt.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return receipt;
        
    }

    @Override
    public List<Receipt> getAll() {
    Receipt receipt = new Receipt();
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                receipt = Receipt.fromRS(rs);
                list.add(receipt);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Receipt> getAll(String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Receipt> getAll(String where, String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;    
    }
    
    @Override
    public Receipt save(Receipt obj) {
        String sql = "INSERT INTO RECEIPT (BR_ID, EMP_ID, CUS_ID,PT_ID, R_DATE,R_TIME, R_TOTAL, R_DISCOUNT, R_MONEYRECEIVED, R_CHANGE, R_PAYMENT,R_NETTOTAL)"
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBranchId());
            stmt.setInt(2, obj.getEmployeeId());
            stmt.setInt(3, obj.getCustomerId());
            stmt.setInt(4, obj.getPromotionId());
            stmt.setString(5, obj.getDate());
            stmt.setString(6, obj.getTime());
            stmt.setFloat(7, obj.getTotal());
            stmt.setFloat(8, obj.getDiscount());
            stmt.setFloat(9, obj.getMoneyReceived());
            stmt.setFloat(10, obj.getChange());
            stmt.setString(11, obj.getPayment());
            stmt.setFloat(12, obj.getNetTotal());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;    
    }

    @Override
    public Receipt update(Receipt obj) {
        String sql = "UPDATE RECEIPT"
                + "SET BR_ID= ?, EMP_ID= ?, CUS_ID= ?,PT_ID= ?, R_DATE= ?,R_TIME= ?, R_TOTAL= ?, R_DISCOUNT= ?, R_MONEYRECEIVED= ?, R_CHANGE= ?, R_PAYMENT= ?,R_NETTOTAL= ?"
                + " WHERE R_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBranchId());
            stmt.setInt(2, obj.getEmployeeId());
            stmt.setInt(3, obj.getCustomerId());
            stmt.setInt(4, obj.getPromotionId());
            stmt.setString(5, obj.getDate());
            stmt.setString(6, obj.getTime());
            stmt.setFloat(7, obj.getTotal());
            stmt.setFloat(8, obj.getDiscount());
            stmt.setFloat(9, obj.getMoneyReceived());
            stmt.setFloat(10, obj.getChange());
            stmt.setString(11, obj.getPayment());
            stmt.setFloat(12, obj.getNetTotal());
            stmt.setInt(13, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;    
        }
    }

    @Override
    public int delete(Receipt obj) {
        String sql = "DELETE FROM RECEIPT WHERE R_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1; 
    }

    @Override
    public Receipt insert(Receipt obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    
    
}

    

