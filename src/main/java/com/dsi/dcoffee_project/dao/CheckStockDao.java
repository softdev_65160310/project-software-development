/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.dao;

import com.dsi.dcoffee_project.helper.DatabaseHelper;
import com.dsi.dcoffee_project.model.CheckStock;
import com.dsi.dcoffee_project.model.CheckStockIngredientDetail;
import com.dsi.dcoffee_project.model.Employee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Surap
 */
public class CheckStockDao implements Dao<CheckStock> {
    
    @Override
    public CheckStock get(int order) {
        CheckStock cs = null;
        String sql = "SELECT * FROM CHECKSTOCK WHERE CHK_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, order);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                cs = CheckStock.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return cs;
    }

    @Override
    public List<CheckStock> getAll() {
        ArrayList<CheckStock> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCK";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStock checkStock = CheckStock.fromRS(rs);
                list.add(checkStock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckStock> getAll(String where, String order) {
        ArrayList<CheckStock> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCK where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStock checkStock = CheckStock.fromRS(rs);
                list.add(checkStock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckStock> getAll(String order) {
        ArrayList<CheckStock> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCK ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStock checkStock = CheckStock.fromRS(rs);
                list.add(checkStock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    

    @Override
    public CheckStock save(CheckStock obj) {
        String sql = "INSERT INTO CHECKSTOCK ( EMP_ID , BR_ID , CHK_DATE ,CHK_TIME)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setInt(2, obj.getBranchId());
            stmt.setString(3, obj.getDate());
            stmt.setString(4,obj.getTime());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckStock update(CheckStock obj) {
        String sql = "UPDATE CHECKSTOCK "
            + "SET EMP_ID = ?, BR_ID = ?, CHK_DATE = ?,CHK_TIME=?"
            + "WHERE CHK_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setInt(2, obj.getBranchId());
            stmt.setString(3, obj.getDate());
            stmt.setString(4,obj.getTime());
            stmt.setInt(5,obj.getId());
            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    

    @Override
    public int delete(CheckStock obj) {
        String sql = "DELETE FROM CHECKSTOCK WHERE CHK_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;   
    }

    @Override
    public CheckStock insert(CheckStock obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    
    
}
