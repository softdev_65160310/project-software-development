/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.dao;

import com.dsi.dcoffee_project.helper.DatabaseHelper;
import com.dsi.dcoffee_project.model.Ingredient;
import com.dsi.dcoffee_project.model.Ingredient;
import com.dsi.dcoffee_project.model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author BAM
 */
public class IngredientDao implements Dao<Ingredient> {
    @Override
    public Ingredient get(int id) {
        Ingredient ingredient = null;
        String sql = "SELECT * FROM INGREDIENT WHERE IND_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ingredient = Ingredient.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ingredient;
    }

    public Ingredient getByName(String name) {
        Ingredient ingredient = null;
        String sql = "SELECT * FROM INGREDIENT WHERE IND_NAME=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ingredient = Ingredient.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ingredient;
    }
    
    @Override
    public List<Ingredient> getAll() {
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredient ingredient = Ingredient.fromRS(rs);
                list.add(ingredient);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Ingredient> getAll(String where, String order) {
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENT where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredient ingredient = Ingredient.fromRS(rs);
                list.add(ingredient);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Ingredient> getAll(String order) {
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENT ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredient ingredient = Ingredient.fromRS(rs);
                list.add(ingredient);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public Product getById(int id) {
        Product product = null;
        String sql = "SELECT * FROM INGREDIENT WHERE IND_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                product = Product.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return product;
    }
    

    @Override
    public Ingredient save(Ingredient obj) {

        String sql = "INSERT INTO INGREDIENT (IND_NAME, IND_PRICE, IND_QUANTITY, IND_MINIMUM, IND_UNIT)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setFloat(2, obj.getPrice());
            stmt.setInt(3,obj.getQty());
            stmt.setInt(4, obj.getMin());
            stmt.setString(5, obj.getUnit());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Ingredient update(Ingredient obj) {
        String sql = "UPDATE INGREDIENT"
                + " SET IND_NAME = ?, IND_PRICE = ?, IND_QUANTITY = ?, IND_MINIMUM = ?, IND_UNIT = ?"
                + " WHERE IND_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setFloat(2, obj.getPrice());
            stmt.setInt(3,obj.getQty());
            stmt.setInt(4, obj.getMin());
            stmt.setString(5, obj.getUnit());
            stmt.setInt(6,obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Ingredient obj) {
        String sql = "DELETE FROM INGREDIENT WHERE IND_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

    @Override
    public Ingredient insert(Ingredient obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
