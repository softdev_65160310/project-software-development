/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.dao;

import com.dsi.dcoffee_project.helper.DatabaseHelper;
import com.dsi.dcoffee_project.model.CheckStockIngredientDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Surap
 */
public class CheckStockIngredientDetailDao implements Dao<CheckStockIngredientDetail> {

    @Override
    public CheckStockIngredientDetail get(int id) {
        CheckStockIngredientDetail checkStockIngredientDetail = null;
        String sql = "SELECT * FROM CHECKSTOCKINGREDIENTDETAIL WHERE CHKD_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkStockIngredientDetail = CheckStockIngredientDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkStockIngredientDetail;
    }

    @Override
    public List<CheckStockIngredientDetail> getAll() {
        ArrayList<CheckStockIngredientDetail> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCKINGREDIENTDETAIL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockIngredientDetail checkStockIngredientDetail = CheckStockIngredientDetail.fromRS(rs);
                list.add(checkStockIngredientDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckStockIngredientDetail> getAll(String where, String order) {
        ArrayList<CheckStockIngredientDetail> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCKINGREDIENTDETAIL where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockIngredientDetail checkStockIngredientDetail = CheckStockIngredientDetail.fromRS(rs);
                list.add(checkStockIngredientDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckStockIngredientDetail> getAll(String order) {
        ArrayList<CheckStockIngredientDetail> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCKINGREDIENTDETAIL ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockIngredientDetail checkStockIngredientDetail = CheckStockIngredientDetail.fromRS(rs);
                list.add(checkStockIngredientDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckStockIngredientDetail> getCSIDbyCheckID(int order) {
        ArrayList<CheckStockIngredientDetail> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCKINGREDIENTDETAIL WHERE CHK_ID = ? ORDER BY CHKD_ID ASC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, order);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                CheckStockIngredientDetail checkStockIngredientDetail = CheckStockIngredientDetail.fromRS(rs);
                list.add(checkStockIngredientDetail);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckStockIngredientDetail save(CheckStockIngredientDetail obj) {
        String sql = "INSERT INTO CHECKSTOCKINGREDIENTDETAIL ( CHK_ID, IND_ID, CHKD_LAST, CHKD_REMAIN)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCheckStockId());
            stmt.setInt(2, obj.getIngredeintId());
            stmt.setInt(3, obj.getCheckLast());
            stmt.setInt(4, obj.getCheckRemain());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckStockIngredientDetail update(CheckStockIngredientDetail obj) {
        String sql = "UPDATE CHECKSTOCKINGREDIENTDETAIL "
                + "SET CHK_ID = ?, IND_ID = ?, CHKD_LAST = ?, CHKD_REMAIN = ? "
                + "WHERE CHKD_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCheckStockId());
            stmt.setInt(2, obj.getIngredeintId());
            stmt.setInt(3, obj.getCheckLast());
            stmt.setInt(4, obj.getCheckRemain());

            stmt.setInt(5, obj.getId());
            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckStockIngredientDetail obj) {
        String sql = "DELETE FROM CHECKSTOCKINGREDIENTDETAIL WHERE CHKD_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public CheckStockIngredientDetail insert(CheckStockIngredientDetail obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
