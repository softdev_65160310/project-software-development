package com.dsi.dcoffee_project.dao;

import com.dsi.dcoffee_project.helper.DatabaseHelper;
import com.dsi.dcoffee_project.model.PromotionBranch;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author BAM
 */
public class PromotionBranchDao implements Dao<PromotionBranch> {
    
    @Override
    public PromotionBranch get(int id) {
        PromotionBranch promotionBranch = null;
        String sql = "SELECT * FROM PROMOTIONBRANCH WHERE PTB_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                promotionBranch = PromotionBranch.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotionBranch;
    }

    @Override
    public List<PromotionBranch> getAll() {
        ArrayList<PromotionBranch> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTIONBRANCH";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PromotionBranch promotionBranch = PromotionBranch.fromRS(rs);
                list.add(promotionBranch);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<PromotionBranch> getAll(String where, String order) {
        ArrayList<PromotionBranch> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTIONBRANCH where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PromotionBranch promotionBranch = PromotionBranch.fromRS(rs);
                list.add(promotionBranch);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<PromotionBranch> getAll(String order) {
        ArrayList<PromotionBranch> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTIONBRANCH ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PromotionBranch promotionBranch = PromotionBranch.fromRS(rs);
                list.add(promotionBranch);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public PromotionBranch getById(int id) {
        PromotionBranch promotionBranch = null;
        String sql = "SELECT * FROM PROMOTIONBRANCH WHERE PTB_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                promotionBranch = PromotionBranch.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotionBranch;
    }

    @Override
    public PromotionBranch save(PromotionBranch obj) {

        String sql = "INSERT INTO PROMOTIONBRANCH (BR_ID, PT_ID)"
                + "VALUES(?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBranchId());
            stmt.setInt(2, obj.getPromotionId());
           
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public PromotionBranch update(PromotionBranch obj) {
        String sql = "UPDATE PROMOTIONBRANCH"
                + " SET BR_ID = ?, PT_ID = ?"
                + " WHERE PTB_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBranchId());
            stmt.setInt(2, obj.getPromotionId());
            stmt.setInt(3, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(PromotionBranch obj) {
        String sql = "DELETE FROM PROMOTIONBRANCH WHERE PTB_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

    @Override
    public PromotionBranch insert(PromotionBranch obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
