package com.dsi.dcoffee_project.dao;

import com.dsi.dcoffee_project.helper.DatabaseHelper;
import com.dsi.dcoffee_project.model.WorkTime;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class WorkTimeDao implements Dao<WorkTime> {

    @Override
    public WorkTime get(int id) {
        WorkTime workTime = new WorkTime();
        String sql = "SELECT * FROM WORKTIME WHERE W_ID =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                workTime = WorkTime.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return workTime;
    }
    
    public WorkTime getByDateEmp(String date,int empId) {
        WorkTime workTime = new WorkTime();
        String sql = "SELECT * FROM WORKTIME WHERE W_DATE =? AND EMP_ID =? ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, date);
            stmt.setInt(2, empId);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                workTime = WorkTime.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return workTime;
    }

    @Override
    public List<WorkTime> getAll() {
        WorkTime workTime;
        ArrayList<WorkTime> list = new ArrayList();
        String sql = "SELECT * FROM WORKTIME";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                workTime = WorkTime.fromRS(rs);
                list.add(workTime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public WorkTime insert(WorkTime obj) {
        String sql = "INSERT INTO WORKTIME (EMP_ID, W_IN_TIME, W_OUT_TIME, W_TOTAL_TIME, W_STATUS,W_OT,W_DATE) "
                + "VALUES(?, ?, ?, ?, ?, ?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, obj.getEmployee());
            stmt.setString(2, obj.getCheckIn());
            stmt.setString(3, obj.getCheckOut());
            stmt.setInt(4, obj.getTotalWorked());
            stmt.setString(5, obj.getStatus());
            stmt.setInt(6, obj.getOvertime());
            stmt.setString(7, obj.getDate());
            stmt.executeUpdate();
//            int id = DatabaseHelper.getInsertedId(stmt);
//            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public WorkTime update(WorkTime obj) {
        String sql = "UPDATE WORKTIME"
                + " SET EMP_ID = ?,W_DATE = ?, W_IN_TIME = ?, W_OUT_TIME = ?, W_TOTAL_TIME = ?, W_STATUS = ?,W_OT = ?"
                + " WHERE W_ID = ? ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee());
            stmt.setString(2, obj.getDate());
            stmt.setString(3, obj.getCheckIn());
            stmt.setString(4, obj.getCheckOut());
            stmt.setInt(5, obj.getTotalWorked());
            stmt.setString(6, obj.getStatus());
            stmt.setInt(7, obj.getOvertime());
            stmt.setInt(8, obj.getId());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public void updateCheckOut(WorkTime obj) {
        String sql = "UPDATE WORKTIME"
                + " SET W_OUT_TIME = ?, W_TOTAL_TIME= ?, W_STATUS = ?,W_OT=?"
                + " WHERE EMP_ID = ? AND W_DATE = ?";
        Connection conn;
        PreparedStatement stmt;
        try {
            conn = DatabaseHelper.getConnect();
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCheckOut());
            stmt.setInt(2, obj.getTotalWorked());
            stmt.setString(3, obj.getStatus());
            stmt.setInt(4, obj.getOvertime());
            stmt.setInt(5, obj.getEmployee());
            stmt.setString(6,obj.getDate());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public int delete(WorkTime obj) {
        String sql = "DELETE FROM WORKTIME WHERE W_ID =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<WorkTime> getAll(String where, String order) {
        ArrayList<WorkTime> list = new ArrayList();
        String sql = "SELECT * FROM WORKTIME ORDER BY " + where + " " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WorkTime workTime = WorkTime.fromRS(rs);
                list.add(workTime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public WorkTime getForDeletet(int id, String time) {
        WorkTime workTime = new WorkTime();
        String sql = "SELECT * FROM WORKTIME WHERE EMP_ID =? AND W_IN_TIME = ? ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.setString(2, time);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                workTime = workTime.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return workTime;
    }
    
    public WorkTime checkOutById(int id) {
        WorkTime workTime = new WorkTime();
        String sql = "SELECT * FROM WORKTIME WHERE W_ID =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                workTime = workTime.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return workTime;
    }

    @Override
    public WorkTime save(WorkTime obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
