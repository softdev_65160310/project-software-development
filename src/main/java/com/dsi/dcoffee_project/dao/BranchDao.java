package com.dsi.dcoffee_project.dao;

import com.dsi.dcoffee_project.helper.DatabaseHelper;
import com.dsi.dcoffee_project.model.Branch;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author BAM
 */
public class BranchDao implements Dao<Branch> {
    
    @Override
    public Branch get(int id) {
        Branch branch = null;
        String sql = "SELECT * FROM BRANCH WHERE BR_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                branch = Branch.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return branch;
    }
    
    public Branch getByName(String name) {
        Branch branch = null;
        String sql = "SELECT * FROM BRANCH WHERE BR_NAME=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                branch = Branch.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return branch;
    }

    @Override
    public List<Branch> getAll() {
        ArrayList<Branch> list = new ArrayList();
        String sql = "SELECT * FROM BRANCH";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Branch branch = Branch.fromRS(rs);
                list.add(branch);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Branch> getAll(String where, String order) {
        ArrayList<Branch> list = new ArrayList();
        String sql = "SELECT * FROM BRANCH where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Branch branch = Branch.fromRS(rs);
                list.add(branch);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Branch> getAll(String order) {
        ArrayList<Branch> list = new ArrayList();
        String sql = "SELECT * FROM BRANCH ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Branch branch = Branch.fromRS(rs);
                list.add(branch);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public Branch getById(int id) {
        Branch branch = null;
        String sql = "SELECT * FROM BRANCH WHERE BR_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                branch = Branch.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return branch;
    }

    @Override
    public Branch save(Branch obj) {

        String sql = "INSERT INTO BRANCH (BR_NAME, BR_ADDRESS, BR_PHONE, BR_ZIPCODE)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getBranchName());
            stmt.setString(2, obj.getBranchAddress());
            stmt.setString(3, obj.getBranchPhone());
            stmt.setString(4, obj.getBranchZipcode());
           
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Branch update(Branch obj) {
        String sql = "UPDATE BRANCH"
                + " SET BR_NAME = ?, BR_ADDRESS = ?, BR_PHONE = ?, BR_ZIPCODE = ?"
                + " WHERE BR_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getBranchName());
            stmt.setString(2, obj.getBranchAddress());
            stmt.setString(3, obj.getBranchPhone());
            stmt.setString(4, obj.getBranchZipcode());
            stmt.setInt(5, obj.getId());
            
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Branch obj) {
        String sql = "DELETE FROM BRANCH WHERE BR_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

    @Override
    public Branch insert(Branch obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
