/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.dao;

import com.dsi.dcoffee_project.helper.DatabaseHelper;
import com.dsi.dcoffee_project.model.Web;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class WebDao implements Dao<Web>{
    @Override
    public Web get(int id) {
        Web web = null;
        String sql = "SELECT * FROM WATERELECTRICITYBILL WHERE WEB_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                web = Web.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return web;
    }

    @Override
    public List<Web> getAll() {
        ArrayList<Web> list = new ArrayList();
        String sql = "SELECT * FROM WATERELECTRICITYBILL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Web web = Web.fromRS(rs);
                list.add(web);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Web> getAll(String where, String order) {
        ArrayList<Web> list = new ArrayList();
        String sql = "SELECT * FROM WATERELECTRICITYBILL where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Web web = Web.fromRS(rs);
                list.add(web);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Web> getAll(String order) {
        ArrayList<Web> list = new ArrayList();
        String sql = "SELECT * FROM WATERELECTRICITYBILL ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Web web = Web.fromRS(rs);
                list.add(web);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Web save(Web obj) {

        String sql = "INSERT INTO WATERELECTRICITYBILL (BR_ID, EMP_ID, WEB_DATE, WEB_WATER_UNIT_READ, WEB_ELC_UNIT_READ, WEB_WATER_UNIT_PERMONTH, WEB_ELC_UNIT_PERMONTH, WEB_WATER_PERUNIT, WEB_ELECTRIC_PERUNIT, WEB_TOTAL_WATER, WEB_TOTAL_ELECTRIC, WEB_STATUS)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBranchId());
            stmt.setInt(2, obj.getEmpId()); 
            stmt.setString(3, obj.getDate());
            stmt.setInt(4, obj.getWaterUnitRead());
            stmt.setInt(5, obj.getElcUnitRead());
            stmt.setInt(6, obj.getWaterUnitPerM());
            stmt.setInt(7, obj.getElcUnitPerM());
            stmt.setInt(8, obj.getWaterPerUnit());
            stmt.setInt(9, obj.getElcPerUnit());
            stmt.setInt(10, obj.getTotal_Water());
            stmt.setInt(11, obj.getTotal_Electric());
            
            stmt.setString(12, obj.getStatus());
            
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Web update(Web obj) {
        String sql = "UPDATE WATERELECTRICITYBILL"
                + " SET BR_ID = ?, EMP_ID = ?, WEB_DATE = ?, WEB_WATER_UNIT_READ = ?, WEB_ELC_UNIT_READ = ?, WEB_WATER_UNIT_PERMONTH = ?, WEB_ELC_UNIT_PERMONTH = ?, WEB_WATER_PERUNIT = ?, WEB_ELECTRIC_PERUNIT = ?, WEB_TOTAL_WATER = ?, WEB_TOTAL_ELECTRIC = ?, WEB_STATUS = ?"
                + " WHERE WEB_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBranchId());
            stmt.setInt(2, obj.getEmpId()); 
            stmt.setString(3, obj.getDate());
            stmt.setInt(4, obj.getWaterUnitRead());
            stmt.setInt(5, obj.getElcUnitRead());
            stmt.setInt(6, obj.getWaterUnitPerM());
            stmt.setInt(7, obj.getElcUnitPerM());
            stmt.setInt(8, obj.getWaterPerUnit());
            stmt.setInt(9, obj.getElcPerUnit());
            stmt.setInt(10, obj.getTotal_Water());
            stmt.setInt(11, obj.getTotal_Electric());
            
            stmt.setString(12, obj.getStatus());
            stmt.setInt(13, obj.getId());
            
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Web obj) {
        String sql = "DELETE FROM WATERELECTRICITYBILL WHERE WEB_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

    @Override
    public Web insert(Web obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
