/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.dao;

import com.dsi.dcoffee_project.helper.DatabaseHelper;
import com.dsi.dcoffee_project.model.IngredientReceipt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class IngredientReceiptDao implements Dao<IngredientReceipt> {

    @Override
    public IngredientReceipt get(int id) {
        IngredientReceipt indrec = null;
        String sql = "SELECT * FROM INGREDIENTRECEIPT WHERE IGRT_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                indrec = IngredientReceipt.fromRS(rs);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return indrec;
    }
    

    @Override
    public List<IngredientReceipt> getAll() {
        IngredientReceipt indrec = new IngredientReceipt();
        ArrayList<IngredientReceipt> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENTRECEIPT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                indrec = indrec.fromRS(rs);
                list.add(indrec);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list; 
    }

    @Override
    public IngredientReceipt save(IngredientReceipt obj) {
        String sql = "INSERT INTO INGREDIENTRECEIPT ( EMP_ID ,VD_ID ,IGRT_DATE ,IGRT_TOTAL ,IGRT_TOTAL_LIST ) VALUES (?,?,?,?,?);";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getEmployeeId());
            stmt.setInt(2, obj.getVendorId());
            stmt.setString(3,obj.getIgrtDate());
            stmt.setFloat(4, obj.getIgrtTotal());
            stmt.setInt(5, obj.getIgrtTotalList());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setIgrt_id(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
        
    }

    @Override
    public IngredientReceipt insert(IngredientReceipt obj) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public IngredientReceipt update(IngredientReceipt obj) {
        String sql = "UPDATE INGREDIENTRECEIPT SET EMP_ID = ?, VD_ID = ?, IGRT_DATE = ? ,IGRT_TOTAL = ?,IGRT_TOTAL_LIST = ? WHERE IGRT_ID = ?;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getEmployeeId());
            stmt.setInt(2, obj.getVendorId());
            stmt.setString(3,obj.getIgrtDate());
            stmt.setFloat(4,obj.getIgrtTotal());
            stmt.setInt(5,obj.getIgrtTotalList());
            stmt.setInt(6,obj.getIgrt_id());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(IngredientReceipt obj) {
        String sql = "DELETE FROM INGREDIENTRECEIPT WHERE IGRT_ID =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIgrt_id());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<IngredientReceipt> getAll(String where, String order) {
        ArrayList<IngredientReceipt> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENTRECEIPT where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                IngredientReceipt indrec = IngredientReceipt.fromRS(rs);
                list.add(indrec);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
}
