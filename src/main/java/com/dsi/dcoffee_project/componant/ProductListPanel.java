/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.dsi.dcoffee_project.componant;

import com.dsi.dcoffee_project.model.Product;
import com.dsi.dcoffee_project.service.ProductService;
import java.awt.GridLayout;
import java.util.ArrayList;

/**
 *
 * @author BoM
 */
public class ProductListPanel extends javax.swing.JPanel implements BuyProductable {

    private ProductService productService;
    private ArrayList<Product> products;
    private ArrayList<BuyProductable> subscribers = new ArrayList<>();
    public static String category;

    /**
     * Creates new form ProductListPanel
     */
    public ProductListPanel() {
        initComponents();
        getProduct();
        
        
        
    }

    private void getProduct() {
        productService = new ProductService();
        if (category == "All") {
            products = productService.getAll();
        } else {
            products = productService.getProductByCategory(category);
        }
        int productSize = products.size();
        for (Product p : products) {
            ProductItemPanel pnlProductItem = new ProductItemPanel(p);
            pnlProductItem.addOnBuyProduct(this);
            pnlProductList.add(pnlProductItem);
        }
//        pnlProductList.setLayout(new GridLayout(productSize,3,0,0));
        pnlProductList.setLayout(new GridLayout((productSize / 3) + ((productSize % 3 != 0) ? 1 : 0), 3, 0, 0));
    }

    public static void setCategory(String category) {
        ProductListPanel.category = category;
    }

    public void addOnBuyProduct(BuyProductable subscriber) {
        subscribers.add(subscriber);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlProductList = new javax.swing.JPanel();

        javax.swing.GroupLayout pnlProductListLayout = new javax.swing.GroupLayout(pnlProductList);
        pnlProductList.setLayout(pnlProductListLayout);
        pnlProductListLayout.setHorizontalGroup(
            pnlProductListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 485, Short.MAX_VALUE)
        );
        pnlProductListLayout.setVerticalGroup(
            pnlProductListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 446, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlProductList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlProductList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel pnlProductList;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, int qty) {
        System.out.println("" + product.getName() + " " + qty);
        for (BuyProductable s : subscribers) {
            s.buy(product, qty);
        }

    }
}
