/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.system;

import java.util.ArrayList;
import java.util.function.Predicate;
import javax.swing.JTextField;

/**
 *
 * @author aotto
 */
public class SystemSearch<T> {
    
    private ArrayList<T> current;
    private final ArrayList<T> dataList;

    public SystemSearch(JTextField component, ArrayList<T> dataList) {
        this.dataList = dataList;
        this.current = dataList;
    }

    public void update(String searchName, Predicate<T> localFunc) {
        current = fetchResult(searchName, localFunc);
        // Notify listeners or update your UI components with the new search results
    }

    private ArrayList<T> fetchResult(String searchName, Predicate<T> localFunc) {
        if (searchName.isEmpty()) {
            return dataList;
        } else {
            ArrayList<T> filtered = new ArrayList<>();
            for (T item : dataList) {
                if (localFunc.test(item)) {
                    filtered.add(item);
                }
            }
            return filtered;
        }
    }

    public ArrayList<T> getResults() {
        return current;
    }
}
