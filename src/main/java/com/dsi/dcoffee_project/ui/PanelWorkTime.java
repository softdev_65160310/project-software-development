/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.dsi.dcoffee_project.ui;

import com.dsi.dcoffee_project.componant.CommonDateTime;
import com.dsi.dcoffee_project.model.Employee;
import com.dsi.dcoffee_project.model.WorkTime;
import com.dsi.dcoffee_project.service.EmployeeService;
import com.dsi.dcoffee_project.service.WorkTimeService;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ASUS
 */
public class PanelWorkTime extends javax.swing.JPanel {
    private final WorkTimeService workTimeService;
    private ArrayList<WorkTime> listWorkTime;
    private LocalTime timeCheckIn;
    private LocalTime timeCheckOut;
    private WorkTime editedWorkTime;
    private Employee employeew = new Employee();
    private Duration workedHours = Duration.ZERO;
    private List<WorkTime> list;
    WorkTime wch;
    
    
    public PanelWorkTime(Employee employee) {
        initComponents();
        showDate();
        showTime();
        employeew = employee;
        System.out.println(employeew);
        workTimeService = new WorkTimeService();
        listWorkTime = workTimeService.getAll();
        initTables();
    }
    
    private int parseInputAsInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            // Handle invalid input here, e.g., display an error message
            return 0; // Default value or any suitable value for invalid input
        }
    }
    
    private void initTables() {
        String[] filterOptions = {"Id", "Date", "EmpID"};

        WorktimeTable.setModel(new AbstractTableModel() {
            String[] headers = {
                "Employee name",
                "Date",
                "Employee ID",
                "Checkin",
                "Checkout",
                "Total Worked",
                "Status",
                "Overtime"
                };

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return listWorkTime.size();
            }

            @Override
            public int getColumnCount() {
                return headers.length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                WorkTime w = listWorkTime.get(rowIndex);
                EmployeeService empSer = new EmployeeService();
                return switch (columnIndex) {
                    case 0 -> empSer.getEmployeeById(w.getEmployee()).getEmp_name();
                    case 1 -> w.getDate();
                    case 2 -> w.getEmployee();
                    case 3 -> w.getCheckIn();
                    case 4 -> w.getCheckOut();
                    case 5 -> w.getTotalWorked();
                    case 6 -> w.getStatus();
                    case 7 -> w.getOvertime();
                    default -> "unknow";
                };
            }

        });
    }
    
    
    void showDate() {
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date d = new java.util.Date();
        jDate.setText(s.format(d));
    }

    void showTime() {
        new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                java.util.Date d = new java.util.Date();
                jTime.setText(sdf.format(d));
            }
        }).start();
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jTime = new javax.swing.JLabel();
        jDate = new javax.swing.JLabel();
        CheckOut = new javax.swing.JButton();
        CheckIn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        WorktimeTable = new javax.swing.JTable();

        setBackground(new java.awt.Color(215, 204, 200));
        setMinimumSize(new java.awt.Dimension(803, 557));
        setPreferredSize(new java.awt.Dimension(803, 557));

        jPanel1.setBackground(new java.awt.Color(215, 204, 200));

        jTime.setFont(new java.awt.Font("Angsana New", 1, 48)); // NOI18N
        jTime.setText("Time");

        jDate.setFont(new java.awt.Font("Angsana New", 1, 48)); // NOI18N
        jDate.setText("Date");

        CheckOut.setBackground(new java.awt.Color(255, 102, 102));
        CheckOut.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        CheckOut.setText("Check Out");
        CheckOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckOutActionPerformed(evt);
            }
        });

        CheckIn.setBackground(new java.awt.Color(102, 255, 102));
        CheckIn.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        CheckIn.setText("Check in");
        CheckIn.setMaximumSize(new java.awt.Dimension(102, 29));
        CheckIn.setMinimumSize(new java.awt.Dimension(102, 29));
        CheckIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckInActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 38)); // NOI18N
        jLabel1.setText("Check in/Check out for Employees");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(CheckIn, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CheckOut, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 628, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jDate, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTime, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap(157, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(81, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckIn, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CheckOut, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jDate, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTime, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        WorktimeTable.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        WorktimeTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        WorktimeTable.setRowHeight(25);
        WorktimeTable.setShowGrid(false);
        WorktimeTable.setShowVerticalLines(true);
        jScrollPane1.setViewportView(WorktimeTable);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 791, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 391, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void CheckInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckInActionPerformed
        wch = new WorkTime(CommonDateTime.getDateNowString(),employeew.getEmp_id(), CommonDateTime.getTimeNowString(), "", 0, "", 0);
        workTimeService.addNew(wch);
        refreshTables();
        CheckOut.setVisible(true);
        CheckIn.setVisible(false);
    }//GEN-LAST:event_CheckInActionPerformed

    private void CheckOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckOutActionPerformed
        
//        WorkTime selectedCheckTime = workTimeService.checkOutById(wch.getId());
//        System.out.println(selectedCheckTime);
//        String timeCheckInString = wch.getCheckIn();
//        
//        
//        
//        
//        
//            
//        
//        selectedCheckTime.setCheckOut(timeCheckOutString);
//        selectedCheckTime.setTotalWorked(totalWorked);
//        selectedCheckTime.setStatus(status);
//        selectedCheckTime.setOvertime(overtime);
//        System.out.println(selectedCheckTime);
//        workTimeService.updateCheckOut(selectedCheckTime);
//        System.out.println(selectedCheckTime);
        
        String timeCheckInString = wch.getCheckIn();
        String timeCheckOutString = CommonDateTime.getTimeNowString();
        timeCheckIn = LocalTime.parse(timeCheckInString, CommonDateTime.getTimeFormatter());
        timeCheckOut = LocalTime.parse(timeCheckOutString, CommonDateTime.getTimeFormatter());
        workedHours = Duration.between(timeCheckIn, timeCheckOut);
        WorkTime worktimeToSelec = workTimeService.getForDelete(wch.getEmployee(),wch.getCheckIn());
        workTimeService.delete(worktimeToSelec);
        int overtime = 0;
        String status;
        int totalWorked = workedHours.toHoursPart();
        if (totalWorked == 8) {
            status = "Present";
        } else if (totalWorked >= 9) {
            status = "Present+OT";
            overtime = totalWorked - 8;
        } else {
            status = "Missing";
        }    
        
        System.out.println(wch);
        wch.setCheckOut(timeCheckOutString);
        wch.setOvertime(overtime);
        wch.setStatus(status);
        wch.setTotalWorked(totalWorked);
        
        workTimeService.addNew(wch);
        
        
        refreshTables();
        CheckOut.setVisible(false);
        CheckIn.setVisible(true);
    }//GEN-LAST:event_CheckOutActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CheckIn;
    private javax.swing.JButton CheckOut;
    private javax.swing.JTable WorktimeTable;
    private javax.swing.JLabel jDate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel jTime;
    // End of variables declaration//GEN-END:variables


    private void refreshTables() {
        listWorkTime = workTimeService.getAll();
        WorktimeTable.revalidate();
        WorktimeTable.repaint();
    }
}