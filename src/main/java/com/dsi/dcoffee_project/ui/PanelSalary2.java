/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.dsi.dcoffee_project.ui;

import com.dsi.dcoffee_project.componant.CommonDateTime;
import com.dsi.dcoffee_project.model.Employee;
import com.dsi.dcoffee_project.model.ReceiptDetail;
import com.dsi.dcoffee_project.model.Salary;
import com.dsi.dcoffee_project.model.WorkTime;
import com.dsi.dcoffee_project.service.EmployeeService;
import com.dsi.dcoffee_project.service.ReceiptDetailService;
import com.dsi.dcoffee_project.service.SalaryService;
import com.dsi.dcoffee_project.service.WorkTimeService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Chantima
 */
public class PanelSalary2 extends javax.swing.JPanel {

    SalaryService slrs = new SalaryService();
    EmployeeService emps = new EmployeeService();
    ArrayList<Salary> salarylist;
    Salary slr;

    /**
     * Creates new form PanelSalary
     */
    public PanelSalary2() {
        initComponents();
        salarylist = slrs.getAll();
        inputEmpNamecmb();
        tblSalary.setModel(new AbstractTableModel() {
            String[] headers = {"Salary ID", "Employee Name", "Salary Date", "Salary Total", "Salary Date Detail", "Status"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return salarylist.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Salary slr = salarylist.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        int slr_id = slr.getId();
                        String formattedemidslr = String.format("SLR%03d", slr_id);
                        return formattedemidslr;
                    case 1:
                        Employee emp = emps.getEmployeeById(slr.getEmployeeId());
                        return emp.getEmp_name();
                    case 2:
                        return slr.getSaralyDate();
                    case 3:
                        return slr.getSalaryTotal();
                    case 4:
                        return slr.getSalaryDateDetail();
                    case 5:
                        String status;
                        if (slr.getSaralyDate().equals("")) {
                            status = "Not Pay";
                        } else {
                            status = "Payed";
                        }
                        return status;
                    default:
                        return "unknown";
                }
            }

        });

    }

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        SalaryDialog1 salaryDialog1 = new SalaryDialog1(frame,slr);
        salaryDialog1.setLocationRelativeTo(this);
        salaryDialog1.setVisible(true);

    }

    private void inputEmpNamecmb() {
        DefaultComboBoxModel<String> modelEmpName = new DefaultComboBoxModel<>();
        WorkTimeService wts = new WorkTimeService();

        List<Employee> listem;
        listem = emps.getEmployee();
        for (int i = 0; i < listem.size(); i++) {
            Employee cmbEmp = listem.get(i);

            String name = cmbEmp.getEmp_name();
            modelEmpName.addElement(name);
        }
        ddEmployee.setModel(modelEmpName);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scrPanel1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblSalary = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        ddEmployee = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        ddMonth = new javax.swing.JComboBox<>();
        ddYear = new javax.swing.JComboBox<>();
        PaymantToSalaryReceipt = new javax.swing.JButton();
        btnCreateSLR = new javax.swing.JButton();
        btnWEB = new javax.swing.JButton();

        scrPanel1.setBorder(null);

        jPanel2.setBackground(new java.awt.Color(215, 204, 200));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        jLabel1.setText("Pay Salaries");

        tblSalary.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Check in", "Check out", "Total Time", "Status", "OT", "Minimum"
            }
        ));
        jScrollPane2.setViewportView(tblSalary);

        jPanel3.setBackground(new java.awt.Color(158, 118, 118));

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Employee Name :");

        ddEmployee.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5" }));
        ddEmployee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ddEmployeeActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Month :");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Year :");

        ddMonth.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddMonth.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" }));
        ddMonth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ddMonthActionPerformed(evt);
            }
        });

        ddYear.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddYear.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089", "2090", "2091", "2092", "2093", "2094", "2095", "2096", "2097", "2098", "2099", "2100" }));
        ddYear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ddYearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ddEmployee, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ddMonth, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ddYear, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(63, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(ddEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5)
                    .addComponent(ddMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ddYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(39, Short.MAX_VALUE))
        );

        PaymantToSalaryReceipt.setBackground(new java.awt.Color(102, 255, 102));
        PaymantToSalaryReceipt.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        PaymantToSalaryReceipt.setText("Payment");
        PaymantToSalaryReceipt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PaymantToSalaryReceiptActionPerformed(evt);
            }
        });

        btnCreateSLR.setBackground(new java.awt.Color(102, 255, 102));
        btnCreateSLR.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnCreateSLR.setText("Create Salary Bill");
        btnCreateSLR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateSLRActionPerformed(evt);
            }
        });

        btnWEB.setBackground(new java.awt.Color(255, 204, 102));
        btnWEB.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnWEB.setText("Water Eletric Bill");
        btnWEB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnWEBActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 642, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(PaymantToSalaryReceipt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnWEB, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnCreateSLR, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE))))
                .addGap(18, 18, 18))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCreateSLR, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnWEB, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(PaymantToSalaryReceipt, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(19, 19, 19))))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        scrPanel1.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrPanel1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrPanel1)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void PaymantToSalaryReceiptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PaymantToSalaryReceiptActionPerformed
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = new java.util.Date();
        String dateSLR = s.format(date);
        List<Salary> listSLR= slrs.getAll();
        int selectedRow = tblSalary.getSelectedRow();
        slr = listSLR.get(selectedRow);
        System.out.println(slr);
        
        System.out.println(slr);
        slr.setSaralyDate(dateSLR);
        slrs.update(slr);
        refreshTable();
        openDialog();
    }//GEN-LAST:event_PaymantToSalaryReceiptActionPerformed

    private void ddEmployeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ddEmployeeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ddEmployeeActionPerformed

    private void ddMonthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ddMonthActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ddMonthActionPerformed

    private void ddYearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ddYearActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ddYearActionPerformed

    private void btnCreateSLRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateSLRActionPerformed
        String empName = (String) ddEmployee.getSelectedItem();
        EmployeeService empSer = new EmployeeService();
        Employee emp = empSer.getEmpByName(empName);
        String month = (String) ddMonth.getSelectedItem();
        String year = (String) ddYear.getSelectedItem();
        WorkTimeService wtSer = new WorkTimeService();
        int totalSlr = 0;
        for (int i = 0; i < 32; i++) {
            String day;
            if (i < 10) {
                day = "0" + i;
            } else {
                day = Integer.toString(i);
            }

            String date = year + "-" + getMonthNumber(month) + "-" + day;
            WorkTime wt = wtSer.getByDateEmp(date,emp.getEmp_id());
            int timePerDay = wt.getTotalWorked();
            totalSlr += timePerDay;
        }
        int wage = (int) emp.getEmp_wage();
        int total = totalSlr * wage;
        slr = new Salary(emp.getEmp_id(),"", total, year+"-"+getMonthNumber(month));
        slrs.addNew(slr);
        System.out.println(slr);
        tblSalary.revalidate();
        tblSalary.repaint();
        refreshTable();
    }//GEN-LAST:event_btnCreateSLRActionPerformed

    private void btnWEBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnWEBActionPerformed
        scrPanel1.setViewportView(new WaterBillPanel());
    }//GEN-LAST:event_btnWEBActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton PaymantToSalaryReceipt;
    private javax.swing.JButton btnCreateSLR;
    private javax.swing.JButton btnWEB;
    private javax.swing.JComboBox<String> ddEmployee;
    private javax.swing.JComboBox<String> ddMonth;
    private javax.swing.JComboBox<String> ddYear;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane scrPanel1;
    private javax.swing.JTable tblSalary;
    // End of variables declaration//GEN-END:variables
public String getMonthNumber(String selectedMonth) {
        if (selectedMonth.equals("January")) {
            return "01";
        } else if (selectedMonth.equals("February")) {
            return "02";
        } else if (selectedMonth.equals("March")) {
            return "03";
        } else if (selectedMonth.equals("April")) {
            return "04";
        } else if (selectedMonth.equals("May")) {
            return "05";
        } else if (selectedMonth.equals("June")) {
            return "06";
        } else if (selectedMonth.equals("July")) {
            return "07";
        } else if (selectedMonth.equals("August")) {
            return "08";
        } else if (selectedMonth.equals("September")) {
            return "09";
        } else if (selectedMonth.equals("October")) {
            return "10";
        } else if (selectedMonth.equals("November")) {
            return "11";
        } else if (selectedMonth.equals("December")) {
            return "12";
        } else {
            // Handle the case when none of the months match
            return "Invalid"; // You can return an error message or a default value as needed
        }
    }
    private void refreshTable() {
        salarylist = slrs.getAll();
        tblSalary.revalidate();
        tblSalary.repaint();
    }
}
