/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.dsi.dcoffee_project.ui;

import com.dsi.dcoffee_project.model.CheckStock;
import com.dsi.dcoffee_project.model.CheckStockIngredientDetail;
import com.dsi.dcoffee_project.model.Employee;
import com.dsi.dcoffee_project.model.Ingredient;
import com.dsi.dcoffee_project.service.BranchService;
import com.dsi.dcoffee_project.service.CheckStockIngredientDetailService;
import com.dsi.dcoffee_project.service.CheckStockService;
import com.dsi.dcoffee_project.service.EmployeeService;
import com.dsi.dcoffee_project.service.IngredientService;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author BAM
 */
public class PanelCheckStockIngredientDetail extends javax.swing.JPanel {

    static void recieveDataPCS(int checkStockID) {
        checkStockID = checkStockId;
    }

    private CheckStockIngredientDetailService csidSer;
    private List<CheckStockIngredientDetail> listCsid;
    private CheckStockIngredientDetail checked_sid;
    public static int checkStockId;

    /**
     * Creates new form Product
     */
    public PanelCheckStockIngredientDetail() {
        initComponents();

        csidSer = new CheckStockIngredientDetailService();

        checkStockId = PanelCheckStock.checkStockID;

        listCsid = csidSer.getCSIDbyCheckID(checkStockId);

        tblCheckStock.setModel(new AbstractTableModel() {
            String[] columnNames = {"No.", "CheckStock ID", "Ingredient", "Check Last", "Check Remain", "Unit"};

            @Override
            public int getRowCount() {
                return listCsid.size();
            }

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getColumnCount() {
                return 6;

            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckStockIngredientDetail csid = listCsid.get(rowIndex);
                IngredientService igSer = new IngredientService();
                Ingredient ig = igSer.getById(csid.getIngredeintId());
                switch (columnIndex) {
                    case 0:
                        int num = rowIndex + 1;
                        return num;
                    case 1:
                        int id = csid.getCheckStockId();
                        String formattedCHKID = String.format("CHK%03d", id);
                        return formattedCHKID;
                    case 2:
                        return ig.getName();
                    case 3:
                        return csid.getCheckLast();
                    case 4:
                        return csid.getCheckRemain();
                    case 5:
                        return ig.getUnit();
                    default:
                        return "";

                }
            }

        });

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scrPanel = new javax.swing.JScrollPane();
        scrSpace = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblCheckStock = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        lblInd = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jButton3 = new javax.swing.JButton();
        btnBack1 = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(800, 500));

        scrPanel.setBackground(new java.awt.Color(255, 204, 204));
        scrPanel.setBorder(null);
        scrPanel.setAutoscrolls(true);
        scrPanel.setEnabled(false);
        scrPanel.setHorizontalScrollBar(null);
        scrPanel.setPreferredSize(new java.awt.Dimension(800, 500));
        scrPanel.setWheelScrollingEnabled(false);

        scrSpace.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        scrSpace.setPreferredSize(new java.awt.Dimension(800, 500));

        tblCheckStock.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        tblCheckStock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblCheckStock.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        jScrollPane2.setViewportView(tblCheckStock);

        jPanel3.setBackground(new java.awt.Color(215, 204, 200));
        jPanel3.setPreferredSize(new java.awt.Dimension(230, 71));

        lblInd.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblInd.setText("CheckStock Ingredient Detail");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(lblInd)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(lblInd)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(215, 204, 200));

        jButton3.setBackground(new java.awt.Color(153, 123, 103));
        jButton3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jButton3.setText("Delete");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        btnBack1.setBackground(new java.awt.Color(153, 123, 103));
        btnBack1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnBack1.setText("Back");
        btnBack1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnBack1)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3)
                    .addComponent(btnBack1))
                .addContainerGap(42, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout scrSpaceLayout = new javax.swing.GroupLayout(scrSpace);
        scrSpace.setLayout(scrSpaceLayout);
        scrSpaceLayout.setHorizontalGroup(
            scrSpaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, scrSpaceLayout.createSequentialGroup()
                .addGroup(scrSpaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 794, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 794, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        scrSpaceLayout.setVerticalGroup(
            scrSpaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(scrSpaceLayout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        scrPanel.setViewportView(scrSpace);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    private void btnBack1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack1ActionPerformed
        scrPanel.setViewportView(new PanelCheckStock());
    }//GEN-LAST:event_btnBack1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        int selectedIndex = tblCheckStock.getSelectedRow();
        if (selectedIndex >= 0) {
            checked_sid = listCsid.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                csidSer.delete(checked_sid);
            }
            refreshTable();
        }
    }//GEN-LAST:event_jButton3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack1;
    private javax.swing.JButton jButton3;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblInd;
    private javax.swing.JScrollPane scrPanel;
    private javax.swing.JPanel scrSpace;
    private javax.swing.JTable tblCheckStock;
    // End of variables declaration//GEN-END:variables

    private void refreshTable() {
        listCsid = csidSer.getCSIDbyCheckID(checkStockId);
        tblCheckStock.revalidate();
        tblCheckStock.repaint();
    }
}
