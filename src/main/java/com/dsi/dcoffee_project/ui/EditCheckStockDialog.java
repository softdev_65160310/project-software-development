/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.dsi.dcoffee_project.ui;

import com.dsi.dcoffee_project.model.Branch;
import com.dsi.dcoffee_project.model.CheckStock;
import com.dsi.dcoffee_project.model.CheckStockIngredientDetail;
import com.dsi.dcoffee_project.model.Employee;
import com.dsi.dcoffee_project.model.Ingredient;
import com.dsi.dcoffee_project.service.BranchService;
import com.dsi.dcoffee_project.service.CheckStockIngredientDetailService;
import com.dsi.dcoffee_project.service.CheckStockService;
import com.dsi.dcoffee_project.service.EmployeeService;
import com.dsi.dcoffee_project.service.IngredientService;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author uSeR
 */
public class EditCheckStockDialog extends javax.swing.JDialog {

    private CheckStockService checkStockService;
    private CheckStock checkedStock;
    private List<Map<String, Object>> dataList = new ArrayList<>();
    private List<Map<String, Object>> newDataList = new ArrayList<>();
    private List<CheckStockIngredientDetail> listCsid;
    private List<CheckStockIngredientDetail> backUpListCsid;
    public static int checkedStockId;

    /**
     * Creates new form AddCheckStockDialog
     */
    public EditCheckStockDialog(java.awt.Frame parent, CheckStock checkedStock) {
        super(parent, true);
        initComponents();
        ImageIcon icon = new ImageIcon("./D-coffeeLOGO.png"); 
        setIconImage(icon.getImage());

        inputIgNameDD();
        inputEmpNameDD();
        inputBranchNameDD();

        checkedStock = setTable();
        setDropDownAndTextBox(checkedStock);
        backUpListCsid = listCsid;

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                callBackUpData();
                dispose();
            }
        });

        tblListCheck.setModel(new AbstractTableModel() {
            String[] columnNames = {"Ingredient", "Quantity", "Unit"};

            @Override
            public int getRowCount() {
                return dataList.size(); // จำนวนแถวตามขนาดของ dataList
            }

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getColumnCount() {
                return 3; // 2 คอลัมน์ (Ingredient, Quantity)
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Map<String, Object> data = dataList.get(rowIndex); // ดึงข้อมูลจาก dataList สำหรับแถวนี้
                switch (columnIndex) {
                    case 0:
                        return data.get("Ingredient"); // แสดง Ingredient
                    case 1:
                        return data.get("Quantity"); // แสดง Quantity
                    case 2:
                        return data.get("Unit");
                    default:
                        return "";
                }
            }
        });

    }

    private CheckStock setTable() {
        CheckStock checkedStock;
        checkStockService = new CheckStockService();
        checkedStockId = PanelCheckStock.checkStockID;
        CheckStockIngredientDetailService csidSer = new CheckStockIngredientDetailService();
        listCsid = csidSer.getCSIDbyCheckID(checkedStockId);
        checkedStock = checkStockService.getById(checkedStockId);
        IngredientService igSer = new IngredientService();
        Ingredient ig;
        for (CheckStockIngredientDetail csidOne : listCsid) {

            ig = igSer.getById(csidOne.getIngredeintId());
            ig.setQty(csidOne.getCheckLast());
            igSer.update(ig);
            String igName = ig.getName();
            int qtyInt = csidOne.getCheckRemain();
            String qty = String.valueOf(qtyInt);
            String igUnit = ig.getUnit();
            Map<String, Object> arrData = new HashMap<>();
            arrData.put("Ingredient", igName);
            arrData.put("Quantity", qty);
            arrData.put("Unit", igUnit);
            dataList.add(arrData);
            System.out.println(csidOne.getCheckLast());

        }
        return checkedStock;
    }

    private void setDropDownAndTextBox(CheckStock checkedStock1) {
        BranchService brSer = new BranchService();
        Branch br = brSer.getById(checkedStock1.getBranchId());
        ddBranch.setSelectedItem(br.getBranchName());
        EmployeeService empSer = new EmployeeService();
        Employee emp = empSer.getEmployeeById(checkedStock1.getEmployeeId());
        ddEmployee.setSelectedItem(emp.getEmp_name());

        //Set Date
        String date = checkedStock1.getDate();
        System.out.println(date);
        String[] partDate = date.split("-");
        ddDay.setSelectedItem(partDate[2]);
        ddMonth.setSelectedItem(getMonthName(partDate[1]));
        ddYear.setSelectedItem(partDate[0]);

        //Set Time
        String time = checkedStock1.getTime();
        String[] partTime = time.split(":");
        String hour = partTime[0];
        String minutes = partTime[1];
        ddHour.setSelectedItem(hour);
        ddMinutes.setSelectedItem(minutes);
    }



    private void inputIgNameDD() {
        DefaultComboBoxModel<String> modelIgName = new DefaultComboBoxModel<>();
        IngredientService igSer = new IngredientService();
        List<Ingredient> listIg;
        listIg = igSer.getIngredients();
        for (int i = 0; i < listIg.size(); i++) {
            Ingredient igNew = listIg.get(i);
            String NameIgNew = igNew.getName();
            modelIgName.addElement(NameIgNew);
        }
        ddIngredient.setModel(modelIgName);
    }

    private void inputEmpNameDD() {
        DefaultComboBoxModel<String> modelEmpName = new DefaultComboBoxModel<>();
        EmployeeService empSer = new EmployeeService();
        List<Employee> listEmp;
        listEmp = empSer.getEmployee();
        for (int i = 0; i < listEmp.size(); i++) {
            Employee empNew = listEmp.get(i);
            String NameEmpNew = empNew.getEmp_name();
            modelEmpName.addElement(NameEmpNew);
        }
        ddEmployee.setModel(modelEmpName);
    }

    private void inputBranchNameDD() {
        DefaultComboBoxModel<String> modelBrName = new DefaultComboBoxModel<>();
        BranchService brSer = new BranchService();
        List<Branch> listBr;
        listBr = brSer.getBranchs();
        for (int i = 0; i < listBr.size(); i++) {
            Branch brNew = listBr.get(i);
            String NameBrNew = brNew.getBranchName();
            modelBrName.addElement(NameBrNew);
        }
        ddBranch.setModel(modelBrName);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        Panel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        ddBranch = new javax.swing.JComboBox<>();
        panelEmpName = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        ddEmployee = new javax.swing.JComboBox<>();
        panelCheckDT = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        ddHour = new javax.swing.JComboBox<>();
        ddMinutes = new javax.swing.JComboBox<>();
        ddDay = new javax.swing.JComboBox<>();
        ddMonth = new javax.swing.JComboBox<>();
        ddYear = new javax.swing.JComboBox<>();
        jPanel3 = new javax.swing.JPanel();
        btnCencel = new javax.swing.JButton();
        btnFinish = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblListCheck = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        txtQty = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        ddIngredient = new javax.swing.JComboBox<>();
        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("CHECKSTOCK DIALOG");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(218, 204, 188));

        Panel1.setBackground(new java.awt.Color(218, 204, 188));

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel3.setText("Branch :");

        ddBranch.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N
        ddBranch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ddBranchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout Panel1Layout = new javax.swing.GroupLayout(Panel1);
        Panel1.setLayout(Panel1Layout);
        Panel1Layout.setHorizontalGroup(
            Panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ddBranch, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        Panel1Layout.setVerticalGroup(
            Panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ddBranch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        panelEmpName.setBackground(new java.awt.Color(218, 204, 188));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel1.setText("Employee Name :");

        ddEmployee.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N
        ddEmployee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ddEmployeeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelEmpNameLayout = new javax.swing.GroupLayout(panelEmpName);
        panelEmpName.setLayout(panelEmpNameLayout);
        panelEmpNameLayout.setHorizontalGroup(
            panelEmpNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEmpNameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ddEmployee, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelEmpNameLayout.setVerticalGroup(
            panelEmpNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEmpNameLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelEmpNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ddEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        panelCheckDT.setBackground(new java.awt.Color(218, 204, 188));

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel5.setText("Check Date :");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel6.setText("Check Time :");

        ddHour.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddHour.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));

        ddMinutes.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddMinutes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60" }));

        ddDay.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddDay.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
        ddDay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ddDayActionPerformed(evt);
            }
        });

        ddMonth.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddMonth.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" }));

        ddYear.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddYear.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089", "2090", "2091", "2092", "2093", "2094", "2095", "2096", "2097", "2098", "2099", "2100" }));

        javax.swing.GroupLayout panelCheckDTLayout = new javax.swing.GroupLayout(panelCheckDT);
        panelCheckDT.setLayout(panelCheckDTLayout);
        panelCheckDTLayout.setHorizontalGroup(
            panelCheckDTLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCheckDTLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addGap(1, 1, 1)
                .addComponent(ddDay, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ddMonth, 0, 1, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ddYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ddHour, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ddMinutes, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        panelCheckDTLayout.setVerticalGroup(
            panelCheckDTLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCheckDTLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCheckDTLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCheckDTLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(ddYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(ddDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(ddMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelCheckDTLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ddHour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(ddMinutes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(218, 204, 188));

        btnCencel.setBackground(new java.awt.Color(255, 102, 102));
        btnCencel.setText("Cancel");
        btnCencel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCencelActionPerformed(evt);
            }
        });

        btnFinish.setBackground(new java.awt.Color(102, 255, 102));
        btnFinish.setText("Finish");
        btnFinish.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFinishActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnFinish, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCencel, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCencel, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFinish, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(218, 204, 188));

        tblListCheck.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Ingredient", "Quantity"
            }
        ));
        jScrollPane2.setViewportView(tblListCheck);

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel7.setText("Ingredient :");

        txtQty.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        jLabel8.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel8.setText("Current Quantity :");

        ddIngredient.setFont(new java.awt.Font("TH SarabunPSK", 0, 16)); // NOI18N
        ddIngredient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ddIngredientActionPerformed(evt);
            }
        });

        btnAdd.setBackground(new java.awt.Color(255, 204, 102));
        btnAdd.setText("ADD");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnDelete.setBackground(new java.awt.Color(255, 102, 102));
        btnDelete.setText("DELETE");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 519, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel7))
                        .addGap(15, 15, 15)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtQty)
                            .addComponent(ddIngredient, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(ddIngredient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(13, 13, 13)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtQty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(btnAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 299, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.setBackground(new java.awt.Color(153, 123, 103));

        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("CheckStock Input");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(161, 161, 161)
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel4)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(panelCheckDT, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelEmpName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(Panel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(panelEmpName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelCheckDT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ddIngredientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ddIngredientActionPerformed

    }//GEN-LAST:event_ddIngredientActionPerformed

    private void btnCencelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCencelActionPerformed
        callBackUpData();
        dispose();
    }//GEN-LAST:event_btnCencelActionPerformed

    public void callBackUpData() {
        CheckStockIngredientDetailService csidSer = new CheckStockIngredientDetailService();
        for(int i =0;i<dataList.size();i++){
            
            csidSer.delete(listCsid.get(i));
        }
        for(int i =0;i<backUpListCsid.size();i++){
            csidSer.addNew(backUpListCsid.get(i));
        }
    }

    private void setBackData() throws NumberFormatException {
        EmployeeService empSer = new EmployeeService();
        Employee empNow = empSer.getEmpByName((String) ddEmployee.getSelectedItem());

        BranchService brSer = new BranchService();
        Branch brNow = brSer.getByName((String) ddBranch.getSelectedItem());

        IngredientService igSer = new IngredientService();
        CheckStockIngredientDetailService csidSer = new CheckStockIngredientDetailService();
        for (int i = 0; i < newDataList.size(); i++) {
            Map<String, Object> data = newDataList.get(i);
            String igNameChk = (String) data.get("Ingredient");
            int igQtyNow = Integer.parseInt((String) data.get("Quantity"));
            Ingredient igChk = igSer.getByName(igNameChk);
            CheckStockIngredientDetail csid = new CheckStockIngredientDetail(checkedStockId, igChk.getId(), igChk.getQty(), igQtyNow);
            csidSer.addNew(csid);
            igChk.setQty(igQtyNow);
            igSer.update(igChk);
        }
    }

    private void ddBranchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ddBranchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ddBranchActionPerformed

    private void ddEmployeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ddEmployeeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ddEmployeeActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        String igInput = (String) ddIngredient.getSelectedItem();
        String qtyInput = txtQty.getText();
        Map<String, Object> arrData = new HashMap<>();
        arrData.put("Ingredient", igInput);
        arrData.put("Quantity", qtyInput);
        IngredientService indSer = new IngredientService();
        Ingredient ind = indSer.getByName(igInput);
        arrData.put("Unit",ind.getUnit());
        dataList.add(arrData);
        newDataList.add(arrData);
        
        refreshTable();
        txtQty.setText("");
    }//GEN-LAST:event_btnAddActionPerformed

    private void refreshTable() {
        tblListCheck.revalidate();
        tblListCheck.repaint();
    }

    private void btnFinishActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFinishActionPerformed
        setBackData();
        dispose();


    }//GEN-LAST:event_btnFinishActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int SelectedIndex = tblListCheck.getSelectedRow();
        if (SelectedIndex >= 0) {
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                dataList.remove(SelectedIndex);
                CheckStockIngredientDetailService csidSer = new CheckStockIngredientDetailService();
                CheckStockIngredientDetail csidDelete = listCsid.get(SelectedIndex);
                csidSer.delete(csidDelete);
            }
            refreshTable();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void ddDayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ddDayActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ddDayActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Panel1;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnCencel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnFinish;
    private javax.swing.JComboBox<String> ddBranch;
    private javax.swing.JComboBox<String> ddDay;
    private javax.swing.JComboBox<String> ddEmployee;
    private javax.swing.JComboBox<String> ddHour;
    private javax.swing.JComboBox<String> ddIngredient;
    private javax.swing.JComboBox<String> ddMinutes;
    private javax.swing.JComboBox<String> ddMonth;
    private javax.swing.JComboBox<String> ddYear;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel panelCheckDT;
    private javax.swing.JPanel panelEmpName;
    private javax.swing.JTable tblListCheck;
    private javax.swing.JTextField txtQty;
    // End of variables declaration//GEN-END:variables
public String getMonthName(String monthNumber) {
        switch (monthNumber) {
            case "01":
                return "January";
            case "02":
                return "February";
            case "03":
                return "March";
            case "04":
                return "April";
            case "05":
                return "May";
            case "06":
                return "June";
            case "07":
                return "July";
            case "08":
                return "August";
            case "09":
                return "September";
            case "10":
                return "October";
            case "11":
                return "November";
            case "12":
                return "December";
            default:
                // Handle the case when the input doesn't match any month
                return "Invalid"; // You can return an error message or a default value as needed
        }
    }

}
