/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.dsi.dcoffee_project.ui;

import com.dsi.dcoffee_project.model.Branch;
import com.dsi.dcoffee_project.model.CheckStock;
import com.dsi.dcoffee_project.model.CheckStockIngredientDetail;
import com.dsi.dcoffee_project.model.Employee;
import com.dsi.dcoffee_project.model.Ingredient;
import com.dsi.dcoffee_project.model.IngredientReceipt;
import com.dsi.dcoffee_project.model.IngredientReceiptDetail;
import com.dsi.dcoffee_project.model.Vendor;
import com.dsi.dcoffee_project.service.BranchService;
import com.dsi.dcoffee_project.service.CheckStockIngredientDetailService;
import com.dsi.dcoffee_project.service.EmployeeService;
import com.dsi.dcoffee_project.service.IngredientReceiptDetailService;
import com.dsi.dcoffee_project.service.IngredientReceiptService;
import com.dsi.dcoffee_project.service.IngredientService;
import com.dsi.dcoffee_project.service.VendorService;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author BAM
 */
public class EditIngredientReceiptDialog extends javax.swing.JDialog {

    private IngredientReceiptService irSer = new IngredientReceiptService();
    //setOnTable
    private List<Map<String, Object>> dataList = new ArrayList<>();
    private List<Map<String, Object>> newDataList = new ArrayList<>();
    //setOnDatabase
    private List<IngredientReceiptDetail> listEditedIRD;

    IngredientReceiptDetailService irdSer = new IngredientReceiptDetailService();
    private float total;
    private int totalList = 0;
    private int IgrtId;

    /**
     * Creates new form IngredientReceiptDialog
     */
    public EditIngredientReceiptDialog(java.awt.Frame parent, IngredientReceipt ingredientReceipt) {
        super(parent, true);
        initComponents();
        
        initEmp();
        initVnd();
        initIngredient();
        ImageIcon icon = new ImageIcon("./D-coffeeLOGO.png"); 
        setIconImage(icon.getImage());
        IgrtId = PanelIngredientReceipt.IGRT_ID;

        callData();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                backupData();
                dispose();
            }
        });

        tblIndReceipt.setModel(new AbstractTableModel() {
            String[] columnNames = {"Ingredient", "Quantity", "Unit", "Price"};

            @Override
            public int getRowCount() {
                return dataList.size(); // จำนวนแถวตามขนาดของ dataList
            }

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getColumnCount() {
                return 4; // 2 คอลัมน์ (Ingredient, Quantity)
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Map<String, Object> data = dataList.get(rowIndex); // ดึงข้อมูลจาก dataList สำหรับแถวนี้
                switch (columnIndex) {
                    case 0:
                        return data.get("Ingredient"); // แสดง Ingredient
                    case 1:
                        return data.get("Quantity"); // แสดง Quantity
                    case 2:
                        return data.get("Unit");
                    case 3:
                        return data.get("Price");
                    default:
                        return "";
                }
            }
        });

    }

    private void callData() {
        //setEmp
        EmployeeService empSer = new EmployeeService();
        Employee empSelected = empSer.getEmployeeById(irSer.getById(IgrtId).getEmployeeId());
        ddEmp.setSelectedItem(empSelected.getEmp_name());
        //SetVnd
        VendorService vndSer = new VendorService();
        Vendor vndSelected = vndSer.getById(irSer.getById(IgrtId).getVendorId());
        ddVnd.setSelectedItem(vndSelected.getName());
        //SetDate
        String date = irSer.getById(IgrtId).getIgrtDate();
        System.out.println(date);
        String[] partDate = date.split("-");
        ddDay.setSelectedItem(partDate[2]);
        ddMonth.setSelectedItem(getMonthName(partDate[1]));
        ddYear.setSelectedItem(partDate[0]);

        listEditedIRD = irdSer.getByIGRTID(IgrtId);

        IngredientService indSer = new IngredientService();
        for (int i = 0; i < listEditedIRD.size(); i++) {
            IngredientReceiptDetail selectedIRD = listEditedIRD.get(i);
            Map<String, Object> arrData = new HashMap<>();
            Ingredient selectedInd = indSer.getById(selectedIRD.getInd_id());
            arrData.put("Ingredient", selectedInd.getName());
            int backQTY_IND = selectedInd.getQty() - selectedIRD.getIndrecdt_qty();
            selectedInd.setQty(backQTY_IND);
            indSer.update(selectedInd);
            arrData.put("Quantity", selectedIRD.getIndrecdt_qty());
            arrData.put("Unit", selectedInd.getUnit());
            arrData.put("Price", selectedIRD.getIndrecdt_qty() * selectedInd.getPrice());
            arrData.put("MFD", selectedIRD.getIndrecdt_mfd());
            arrData.put("EXP", selectedIRD.getIndrecdt_exp());
            float totalOne = selectedInd.getPrice() * selectedIRD.getIndrecdt_qty();
            total += totalOne;
            totalList = listEditedIRD.size();
            dataList.add(arrData);
        }

        bxTotal.setText(String.valueOf(total));
        bxTotalList.setText(Integer.toString(totalList));

    }

    private void initIngredient() {
        DefaultComboBoxModel<String> modelInd = new DefaultComboBoxModel<>();
        IngredientService indSer = new IngredientService();
        List<Ingredient> listInd;
        listInd = indSer.getIngredients();
        for (int i = 0; i < listInd.size(); i++) {
            Ingredient indN = listInd.get(i);
            String nameVndN = indN.getName();
            modelInd.addElement(nameVndN);
        }
        ddIngredient.setModel(modelInd);
    }

    private void initVnd() {
        DefaultComboBoxModel<String> modelVendor = new DefaultComboBoxModel<>();
        VendorService vndSer = new VendorService();
        List<Vendor> listVnd;
        listVnd = vndSer.getVendor();
        for (int i = 0; i < listVnd.size(); i++) {
            Vendor vndN = listVnd.get(i);
            String nameVndN = vndN.getName();
            modelVendor.addElement(nameVndN);
        }
        ddVnd.setModel(modelVendor);
    }

    private void initEmp() {
        DefaultComboBoxModel<String> modelEmpName = new DefaultComboBoxModel<>();
        EmployeeService empSer = new EmployeeService();
        List<Employee> listEmp;
        listEmp = empSer.getEmployee();
        for (int i = 0; i < listEmp.size(); i++) {
            Employee empNew = listEmp.get(i);
            String NameEmpNew = empNew.getEmp_name();
            modelEmpName.addElement(NameEmpNew);
        }
        ddEmp.setModel(modelEmpName);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        ddEmp = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        ddVnd = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        bxTotal = new javax.swing.JTextField();
        bxTotalList = new javax.swing.JTextField();
        ddDay = new javax.swing.JComboBox<>();
        ddMonth = new javax.swing.JComboBox<>();
        ddYear = new javax.swing.JComboBox<>();
        btnCancle = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtQty = new javax.swing.JTextField();
        ddIngredient = new javax.swing.JComboBox<>();
        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblIndReceipt = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        ddMfdDay = new javax.swing.JComboBox<>();
        ddMfdMonth = new javax.swing.JComboBox<>();
        ddMfdYear = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        ddExpDay = new javax.swing.JComboBox<>();
        ddExpMonth = new javax.swing.JComboBox<>();
        ddExpYear = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("INGREDIENTRECEIPT DIALOG");

        jPanel1.setBackground(new java.awt.Color(218, 204, 188));

        ddEmp.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddEmp.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel2.setText("Employee Name :");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel3.setText("Vendor  :");

        ddVnd.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddVnd.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel4.setText("Date :");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel5.setText("Total :");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel6.setText("Total List :");

        bxTotal.setEditable(false);
        bxTotal.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        bxTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bxTotalActionPerformed(evt);
            }
        });

        bxTotalList.setEditable(false);
        bxTotalList.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        bxTotalList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bxTotalListActionPerformed(evt);
            }
        });

        ddDay.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddDay.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));

        ddMonth.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddMonth.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" }));
        ddMonth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ddMonthActionPerformed(evt);
            }
        });

        ddYear.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddYear.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089", "2090", "2091", "2092", "2093", "2094", "2095", "2096", "2097", "2098", "2099", "2100" }));

        btnCancle.setBackground(new java.awt.Color(255, 102, 102));
        btnCancle.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnCancle.setText("Cancel");
        btnCancle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancleActionPerformed(evt);
            }
        });

        btnSave.setBackground(new java.awt.Color(102, 255, 102));
        btnSave.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(207, 194, 180));

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel7.setText("Ingredient :");

        jLabel8.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel8.setText("Current Quantity :");

        txtQty.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        ddIngredient.setFont(new java.awt.Font("TH SarabunPSK", 0, 16)); // NOI18N
        ddIngredient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ddIngredientActionPerformed(evt);
            }
        });

        btnAdd.setText("ADD");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        tblIndReceipt.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblIndReceipt);

        jLabel9.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel9.setText("Manufactured Date :");

        ddMfdDay.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddMfdDay.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));

        ddMfdMonth.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddMfdMonth.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" }));
        ddMfdMonth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ddMfdMonthActionPerformed(evt);
            }
        });

        ddMfdYear.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddMfdYear.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089", "2090", "2091", "2092", "2093", "2094", "2095", "2096", "2097", "2098", "2099", "2100" }));

        jLabel10.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel10.setText("Expiry Date :");

        ddExpDay.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddExpDay.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));

        ddExpMonth.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddExpMonth.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" }));
        ddExpMonth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ddExpMonthActionPerformed(evt);
            }
        });

        ddExpYear.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ddExpYear.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089", "2090", "2091", "2092", "2093", "2094", "2095", "2096", "2097", "2098", "2099", "2100" }));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(btnAdd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDelete)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel10))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(ddExpDay, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(ddMfdDay, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(5, 5, 5)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ddMfdMonth, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(ddExpMonth, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(ddMfdYear, 0, 112, Short.MAX_VALUE)
                                    .addComponent(ddExpYear, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ddIngredient, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtQty, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(68, 68, 68))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtQty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(ddIngredient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(ddMfdDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ddMfdMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ddMfdYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(ddExpDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ddExpMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ddExpYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnDelete))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ddEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ddVnd, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel4))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(ddDay, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(ddMonth, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(ddYear, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(bxTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel6)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(bxTotalList, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 513, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ddEmp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(28, 28, 28)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(ddVnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(33, 33, 33)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(ddDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ddMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ddYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(33, 33, 33)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(bxTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6)
                            .addComponent(bxTotalList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 144, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnCancle)
                            .addComponent(btnSave)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(153, 123, 103));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Ingredient Receipt Input");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(273, 273, 273)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel1)
                .addContainerGap(32, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancleActionPerformed
        backupData();

    }//GEN-LAST:event_btnCancleActionPerformed

    private void backupData() {
        for (int i = 0; i < listEditedIRD.size(); i++) {
            IngredientReceiptDetail backupIRD = listEditedIRD.get(i);
            try {
                irdSer.delete(backupIRD);
            } catch (Exception e) {
                continue;
            }
            irdSer.addNew(backupIRD);
            IngredientService indSer = new IngredientService();
            Ingredient indBack;
            indBack = indSer.getById(backupIRD.getInd_id());
            int backUpQTY = indBack.getQty() + backupIRD.getIndrecdt_qty();
            indBack.setQty(backUpQTY);
            indSer.update(indBack);
        }

        dispose();
    }

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed

        try {
            for (int i = 0; i < newDataList.size(); i++) {
                Map<String, Object> data = newDataList.get(i);
                IngredientService indSer = new IngredientService();
                String IndName = (String) data.get("Ingredient");
                int IndId = indSer.getByName(IndName).getId();
                String setMFD = (String) data.get("MFD");
                String setEXP = (String) data.get("EXP");
                String inputQtyStr = (String) data.get("Quantity");
                int inputQty = Integer.parseInt(inputQtyStr);
                IngredientReceiptDetail ird = new IngredientReceiptDetail(IndId, IgrtId, setMFD, setEXP, inputQty);
                irdSer.addNew(ird);
                Ingredient indInput = indSer.getById(IndId);
                int currentQty = indInput.getQty();
                indInput.setQty(currentQty + inputQty);
                indSer.update(indInput);

            }
        } catch (Exception e) {
            dispose();
        }
        IngredientReceiptService irSer = new IngredientReceiptService();
        IngredientReceipt irUpdate = irSer.getById(IgrtId);
        irUpdate.setIgrtTotal(Float.parseFloat(bxTotal.getText()));
        irUpdate.setIgrtTotalList(Integer.parseInt(bxTotalList.getText()));
        irSer.update(irUpdate);
        dispose();
        
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSaveActionPerformed

    private void ddMonthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ddMonthActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ddMonthActionPerformed

    private void ddIngredientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ddIngredientActionPerformed

    }//GEN-LAST:event_ddIngredientActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        String igInput = (String) ddIngredient.getSelectedItem();
        String qtyInput = txtQty.getText();
        IngredientService igSer = new IngredientService();
        String igUnit = igSer.getByName(igInput).getUnit();
        int igPrice = (int) igSer.getByName(igInput).getPrice();
        int qtyInt = (int) Integer.parseInt(qtyInput);
        Map<String, Object> arrData = new HashMap<>();
        int total1 = igPrice * qtyInt;
        arrData.put("Ingredient", igInput);
        arrData.put("Quantity", qtyInput);
        arrData.put("Unit", igUnit);
        arrData.put("Price", total1);
        String MFD = (String) ddMfdYear.getSelectedItem() + "-" + getMonthNumber((String) ddMfdMonth.getSelectedItem()) + "-" + ddMfdDay.getSelectedItem();
        arrData.put("MFD", MFD);
        String EXP = (String) ddExpYear.getSelectedItem() + "-" + getMonthNumber((String) ddExpMonth.getSelectedItem()) + "-" + ddExpDay.getSelectedItem();
        arrData.put("EXP", EXP);
        newDataList.add(arrData);
        dataList.add(arrData);
        refreshTable();

        total += total1;
        bxTotal.setText(String.valueOf(total));
        totalList = dataList.size();
        bxTotalList.setText(Integer.toString(totalList));
    }//GEN-LAST:event_btnAddActionPerformed
    private void refreshTable() {
        tblIndReceipt.revalidate();
        tblIndReceipt.repaint();
        txtQty.setText("");
        ddIngredient.requestFocus();
    }
    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int SelectedIndex = tblIndReceipt.getSelectedRow();
        if (SelectedIndex >= 0) {
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                Map<String, Object> selecData = dataList.get(SelectedIndex);
                IngredientService indSer = new IngredientService();
                String igInput = selecData.get("Ingredient").toString();
                int igPrice = (int) indSer.getByName(igInput).getPrice();
                String qtyInput = selecData.get("Quantity").toString();
                int qtyInt = (int) Integer.parseInt(qtyInput);
                int deleteTotal = igPrice * qtyInt;

                dataList.remove(selecData);

                try {
                    int irdId = listEditedIRD.get(SelectedIndex).getIndrecdt_id();
                    IngredientReceiptDetail irdDelete = irdSer.getById(irdId);
                    System.out.println(irdDelete);
                    irdSer.delete(irdDelete);
                } catch (Exception e) {

                }
                refreshTable();
                total = total - deleteTotal;
            }

            bxTotal.setText(String.valueOf(total));
            totalList = tblIndReceipt.getRowCount();
            bxTotalList.setText(Integer.toString(totalList));

        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void bxTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bxTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bxTotalActionPerformed

    private void bxTotalListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bxTotalListActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bxTotalListActionPerformed

    private void ddMfdMonthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ddMfdMonthActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ddMfdMonthActionPerformed

    private void ddExpMonthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ddExpMonthActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ddExpMonthActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSave;
    private javax.swing.JTextField bxTotal;
    private javax.swing.JTextField bxTotalList;
    private javax.swing.JComboBox<String> ddDay;
    private javax.swing.JComboBox<String> ddEmp;
    private javax.swing.JComboBox<String> ddExpDay;
    private javax.swing.JComboBox<String> ddExpMonth;
    private javax.swing.JComboBox<String> ddExpYear;
    private javax.swing.JComboBox<String> ddIngredient;
    private javax.swing.JComboBox<String> ddMfdDay;
    private javax.swing.JComboBox<String> ddMfdMonth;
    private javax.swing.JComboBox<String> ddMfdYear;
    private javax.swing.JComboBox<String> ddMonth;
    private javax.swing.JComboBox<String> ddVnd;
    private javax.swing.JComboBox<String> ddYear;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblIndReceipt;
    private javax.swing.JTextField txtQty;
    // End of variables declaration//GEN-END:variables
public String getMonthNumber(String selectedMonth) {
        if (selectedMonth.equals("January")) {
            return "01";
        } else if (selectedMonth.equals("February")) {
            return "02";
        } else if (selectedMonth.equals("March")) {
            return "03";
        } else if (selectedMonth.equals("April")) {
            return "04";
        } else if (selectedMonth.equals("May")) {
            return "05";
        } else if (selectedMonth.equals("June")) {
            return "06";
        } else if (selectedMonth.equals("July")) {
            return "07";
        } else if (selectedMonth.equals("August")) {
            return "08";
        } else if (selectedMonth.equals("September")) {
            return "09";
        } else if (selectedMonth.equals("October")) {
            return "10";
        } else if (selectedMonth.equals("November")) {
            return "11";
        } else if (selectedMonth.equals("December")) {
            return "12";
        } else {
            // Handle the case when none of the months match
            return "Invalid"; // You can return an error message or a default value as needed
        }
    }

    public String getMonthName(String monthNumber) {
        switch (monthNumber) {
            case "01":
                return "January";
            case "02":
                return "February";
            case "03":
                return "March";
            case "04":
                return "April";
            case "05":
                return "May";
            case "06":
                return "June";
            case "07":
                return "July";
            case "08":
                return "August";
            case "09":
                return "September";
            case "10":
                return "October";
            case "11":
                return "November";
            case "12":
                return "December";
            default:
                // Handle the case when the input doesn't match any month
                return "Invalid"; // You can return an error message or a default value as needed
        }
    }

}
