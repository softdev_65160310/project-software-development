/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class IngredientReceipt {
    private int igrt_id;
    private int EmployeeId;
    private int VendorId;
    private String IgrtDate;
    private float IgrtTotal;
    private int IgrtTotalList;
    
    public IngredientReceipt(int igrt_id, int EmployeeId, int VendorId, String IgrtDate, float IgrtTotal,int IgrtTotalList) {
        this.igrt_id = igrt_id;
        this.EmployeeId = EmployeeId;
        this.VendorId = VendorId;
        this.IgrtDate = IgrtDate;
        this.IgrtTotal = IgrtTotal;
        this.IgrtTotalList = IgrtTotalList;
    }

    public IngredientReceipt(int EmployeeId, int VendorId, String IgrtDate, float IgrtTotal,int IgrtTotalList) {
        this.igrt_id = -1;
        this.EmployeeId = EmployeeId;
        this.VendorId = VendorId;
        this.IgrtDate = IgrtDate;
        this.IgrtTotal = IgrtTotal;
        this.IgrtTotalList = IgrtTotalList;
    }

    public IngredientReceipt() {
        this.igrt_id = -1;
        this.EmployeeId = 0;
        this.VendorId = 0;
        this.IgrtDate = null;
        this.IgrtTotal = 0;
        this.IgrtTotalList = 0;
    }

    public int getIgrt_id() {
        return igrt_id;
    }

    public void setIgrt_id(int igrt_id) {
        this.igrt_id = igrt_id;
    }

    public int getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(int EmployeeId) {
        this.EmployeeId = EmployeeId;
    }

    public int getVendorId() {
        return VendorId;
    }

    public void setVendorId(int VendorId) {
        this.VendorId = VendorId;
    }

    public String getIgrtDate() {
        return IgrtDate;
    }

    public void setIgrtDate(String IgrtDate) {
        this.IgrtDate = IgrtDate;
    }

    public float getIgrtTotal() {
        return IgrtTotal;
    }

    public void setIgrtTotal(float IgrtTotal) {
        this.IgrtTotal = IgrtTotal;
    }

    public int getIgrtTotalList() {
        return IgrtTotalList;
    }

    public void setIgrtTotalList(int IgrtTotalList) {
        this.IgrtTotalList = IgrtTotalList;
    }

    @Override
    public String toString() {
        return "IngredientReceipt{" + "igrt_id=" + igrt_id + ", EmployeeId=" + EmployeeId + ", VendorId=" + VendorId + ", IgrtDate=" + IgrtDate + ", IgrtTotal=" + IgrtTotal + ", IgrtTotalList=" + IgrtTotalList + '}';
    }
    
    public static IngredientReceipt fromRS(ResultSet rs) {
        IngredientReceipt indrec = new IngredientReceipt();
        try {
            indrec.setIgrt_id(rs.getInt("IGRT_ID"));
            indrec.setEmployeeId(rs.getInt("EMP_ID"));
            indrec.setVendorId(rs.getInt("VD_ID"));
            indrec.setIgrtDate(rs.getString("IGRT_DATE"));
            indrec.setIgrtTotal(rs.getFloat("IGRT_TOTAL"));
            indrec.setIgrtTotalList(rs.getInt("IGRT_TOTAL_LIST"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Salary.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return indrec; // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
