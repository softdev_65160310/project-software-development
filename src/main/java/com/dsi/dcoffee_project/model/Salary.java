/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.dsi.dcoffee_project.model;




import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;




import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author jeep
 */
public class Salary {
    private int id;
    private int EmployeeId;
    private String SaralyDate;
    private float SalaryTotal;
    private String SalaryDateDetail;

    public Salary(int id,  int EmployeeId, String SaralyDate, float SalaryTotal, String SalaryDateDetail) {
        this.id = id;
        this.EmployeeId = EmployeeId;
        this.SaralyDate = SaralyDate;
        this.SalaryTotal = SalaryTotal;
        this.SalaryDateDetail = SalaryDateDetail;
    }
    public Salary( int EmployeeId, String SaralyDate, float SalaryTotal, String SalaryDateDetail) {
        this.id = -1;
        this.EmployeeId = EmployeeId;
        this.SaralyDate = SaralyDate;
        this.SalaryTotal = SalaryTotal;
        this.SalaryDateDetail = SalaryDateDetail;
    }
    public Salary( int EmployeeId,  float SalaryTotal, String SalaryDateDetail) {
        this.id = -1;
        this.EmployeeId = EmployeeId;
        this.SalaryTotal = SalaryTotal;
        this.SalaryDateDetail = SalaryDateDetail;
    }
    public Salary() {
        this.id = -1;
        this.EmployeeId = 0;
        this.SaralyDate = null;
        this.SalaryTotal = 0;
        this.SalaryDateDetail = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public int getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(int EmployeeId) {
        this.EmployeeId = EmployeeId;
    }

    public String getSaralyDate() {
        return SaralyDate;
    }

    public void setSaralyDate(String SaralyDate) {
        this.SaralyDate = SaralyDate;
    }

    public float getSalaryTotal() {
        return SalaryTotal;
    }

    public void setSalaryTotal(float SalaryTotal) {
        this.SalaryTotal = SalaryTotal;
    }

    public String getSalaryDateDetail() {
        return SalaryDateDetail;
    }

    public void setSalaryDateDetail(String SalaryDateDetail) {
        this.SalaryDateDetail = SalaryDateDetail;
    }

    @Override
    public String toString() {
        return "Salary{" + "id=" + id + ", EmployeeId=" + EmployeeId + ", SaralyDate=" + SaralyDate + ", SalaryTotal=" + SalaryTotal + ", SalaryDateDetail=" + SalaryDateDetail + '}';
    }


    
    
    public static Salary fromRS(ResultSet rs) {
        Salary salary = new Salary();
        try {
            salary.setId(rs.getInt("SLR_ID"));
            salary.setEmployeeId(rs.getInt("EMP_ID"));
            salary.setSaralyDate(rs.getString("SLR_DATE"));
            salary.setSalaryTotal(rs.getFloat("SLR_TOTAL"));
            salary.setSalaryDateDetail(rs.getString("SLR_DATE_DETAIL"));
         
        } catch (SQLException ex) {
            Logger.getLogger(Salary.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salary;
    }

    public void addNew(Salary s1) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}

