/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.dsi.dcoffee_project.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Tanakorn
 */
public class Promotion {
    private int id;
    private String name;
    private String startDate;
    private String endDate;
    private String status;
    private int discount;

    public Promotion(int id, String name, String startDate, String endDate, String status, int discount) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.discount = discount;
    }
    
    public Promotion(String name, String startDate, String endDate, String status, int discount) {
        this.id = -1;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.discount = discount;
    }
    
    public Promotion() {
        this.id = -1;
        this.name = "";
        this.startDate = "";
        this.endDate = "";
        this.status = "false";
        this.discount = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", startDate=" + startDate + ", endDate=" + endDate + ", status=" + status + ", discount=" + discount + '}';
    }
    
    

    
  
    public static Promotion fromRS(ResultSet rs) {
        Promotion product = new Promotion();
        try {
            product.setId(rs.getInt("PT_ID"));
            product.setName(rs.getString("PT_NAME"));
            product.setStartDate(rs.getString("PT_START_DATE"));
            product.setEndDate(rs.getString("PT_END_DATE"));
            product.setStatus(rs.getString("PT_STATUS"));
            product.setDiscount(rs.getInt("PT_DISCOUNT"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }
}
