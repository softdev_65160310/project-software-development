/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.dsi.dcoffee_project.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Tanakorn
 */
public class PromotionBranch {
    private int id;
    private int branchId;
    private int promotionId;

    public PromotionBranch(int id, int branchId, int promotionId) {
        this.id = id;
        this.branchId = branchId;
        this.promotionId = promotionId;
    }
    
    public PromotionBranch(int branchId, int promotionId) {
        this.id = -1;
        this.branchId = branchId;
        this.promotionId = promotionId;
    }
    
    public PromotionBranch() {
        this.id = -1;
        this.branchId = 0;
        this.promotionId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    @Override
    public String toString() {
        return "PromotionBranch{" + "id=" + id + ", branchId=" + branchId + ", promotionId=" + promotionId + '}';
    }
    
    

    

    
    

    
  
    public static PromotionBranch fromRS(ResultSet rs) {
        PromotionBranch product = new PromotionBranch();
        try {
            product.setId(rs.getInt("PTB_ID"));
            product.setBranchId(rs.getInt("BR_ID"));
            product.setPromotionId(rs.getInt("PT_ID"));
            
        } catch (SQLException ex) {
            Logger.getLogger(PromotionBranch.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }
}
