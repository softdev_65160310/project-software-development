/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.model;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BoM
 */
public class Receipt {
    private int id;
    private int BranchId;
    private int EmployeeId;
    private int CustomerId;
    private int PromotionId;
    private String Date;
    private String Time;
    private float total;
    private float Discount;
    private float MoneyReceived;
    private float Change;
    private String Payment;
    private float netTotal;
    


    public Receipt(int id, int BranchId, int EmployeeId, int CustomerId, int PromotionId, String Date, String Time, float total, float Discount, float MoneyReceived, float Change, String Payment, float netTotal) {
        this.id = id;
        this.BranchId = BranchId;
        this.EmployeeId = EmployeeId;
        this.CustomerId = CustomerId;
        this.PromotionId = PromotionId;
        this.Date = Date;
        this.Time = Time;
        this.total = total;
        this.Discount = Discount;
        this.MoneyReceived = MoneyReceived;
        this.Change = Change;
        this.Payment = Payment;
        this.netTotal = netTotal;
    }

    public Receipt(int BranchId, int EmployeeId, int CustomerId, int PromotionId, String Date, String Time, float total, float Discount, float MoneyReceived, float Change, String Payment, float netTotal) {
        this.id = -1;
        this.BranchId = BranchId;
        this.EmployeeId = EmployeeId;
        this.CustomerId = CustomerId;
        this.PromotionId = PromotionId;
        this.Date = Date;
        this.Time = Time;
        this.total = total;
        this.Discount = Discount;
        this.MoneyReceived = MoneyReceived;
        this.Change = Change;
        this.Payment = Payment;
        this.netTotal = netTotal;
    }
    
    public Receipt() {
        this.id = -1;
        this.BranchId = 0;
        this.EmployeeId = 0;
        this.CustomerId = 0;
        this.PromotionId = 0;
        this.Date = "";
        this.Time = "";
        this.total = 0;
        this.Discount = 0;
        this.MoneyReceived = 0;
        this.Change = 0;
        this.Payment = "";
        this.netTotal = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBranchId() {
        return BranchId;
    }

    public void setBranchId(int BranchId) {
        this.BranchId = BranchId;
    }

    public int getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(int EmployeeId) {
        this.EmployeeId = EmployeeId;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int CustomerId) {
        this.CustomerId = CustomerId;
    }

    public int getPromotionId() {
        return PromotionId;
    }

    public void setPromotionId(int PromotionId) {
        this.PromotionId = PromotionId;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String Time) {
        this.Time = Time;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getDiscount() {
        return Discount;
    }

    public void setDiscount(float Discount) {
        this.Discount = Discount;
    }

    public float getMoneyReceived() {
        return MoneyReceived;
    }

    public void setMoneyReceived(float MoneyReceived) {
        this.MoneyReceived = MoneyReceived;
    }

    public float getChange() {
        return Change;
    }

    public void setChange(float Change) {
        this.Change = Change;
    }

    public String getPayment() {
        return Payment;
    }

    public void setPayment(String Payment) {
        this.Payment = Payment;
    }

    public float getNetTotal() {
        return netTotal;
    }

    public void setNetTotal(float netTotal) {
        this.netTotal = netTotal;
    }

    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", BranchId=" + BranchId + ", EmployeeId=" + EmployeeId + ", CustomerId=" + CustomerId + ", PromotionId=" + PromotionId + ", Date=" + Date + ", Time=" + Time + ", total=" + total + ", Discount=" + Discount + ", MoneyReceived=" + MoneyReceived + ", Change=" + Change + ", Payment=" + Payment + ", netTotal=" + netTotal + '}';
    }
    
    public static Receipt fromRS(ResultSet rs) {
            Receipt receipt = new Receipt();
            try {
                receipt.setId(rs.getInt("R_ID"));
                receipt.setBranchId(rs.getInt("BR_ID"));
                receipt.setEmployeeId(rs.getInt("EMP_ID"));
                receipt.setCustomerId(rs.getInt("CUS_ID"));
                receipt.setPromotionId(rs.getInt("PT_ID"));
                receipt.setDate(rs.getString("R_DATE"));
                receipt.setTime(rs.getString("R_TIME"));
                receipt.setTotal(rs.getInt("R_TOTAL"));
                receipt.setDiscount(rs.getFloat("R_DISCOUNT"));
                receipt.setMoneyReceived(rs.getFloat("R_MONEYRECEIVED"));
                receipt.setChange(rs.getFloat("R_CHANGE"));
                receipt.setPayment(rs.getString("R_PAYMENT"));

            } catch (SQLException ex) {
                Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
            return receipt;
        }

    

   
    }

