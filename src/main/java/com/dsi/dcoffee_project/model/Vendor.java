/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.dsi.dcoffee_project.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Surap
 */
public class Vendor {
    private int id;
    private String name;
    private String address;
    private String tel;
    private String email;

    public Vendor(int id, String name, String address, String tel, String email) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.tel = tel;
        this.email = email;
    }
    
    public Vendor(String name, String address, String tel, String email) {
        this.id = -1;
        this.name = name;
        this.address = address;
        this.tel = tel;
        this.email = email;
    }
    
    public Vendor() {
        this.id = -1;
        this.name = "";
        this.address = "";
        this.tel = "";
        this.email = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Vendor{" + "id=" + id + ", name=" + name + ", address=" + address + ", tel=" + tel + ", email=" + email + '}';
    }
    

    
    public static Vendor fromRS(ResultSet rs) {
        Vendor vendor = new Vendor();
        try {
            vendor.setId(rs.getInt("VD_ID"));
            vendor.setName(rs.getString("VD_NAME"));
            vendor.setAddress(rs.getString("VD_ADDRESS"));
            vendor.setTel(rs.getString("VD_TEL"));
            vendor.setEmail(rs.getString("VD_EMAIL"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Vendor.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return vendor;
    }
}
