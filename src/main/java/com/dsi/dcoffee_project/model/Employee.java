/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author
 */
public class Employee {

    private int emp_id;
    private int br_id;
    private String emp_name;
    private String emp_email;
    private String emp_phone_number;
    private String emp_address;
    private String emp_position;
    private float emp_wage;
    private String emp_type;
    private String emp_password;
    private float emp_salary;

    public Employee(int emp_id, int br_id, String emp_name, String emp_email, String emp_phone_number, String emp_address, String emp_position, float emp_wage, String emp_type, String emp_password,float emp_salary) {
        this.emp_id = emp_id;
        this.br_id = br_id;
        this.emp_name = emp_name;
        this.emp_email = emp_email;
        this.emp_phone_number = emp_phone_number;
        this.emp_address = emp_address;
        this.emp_position = emp_position;
        this.emp_wage = emp_wage;
        this.emp_type = emp_type;
        this.emp_password = emp_password;
        this.emp_salary = emp_salary;
    }

    public Employee(int br_id, String emp_name, String emp_email, String emp_phone_number, String emp_address, String emp_position, float emp_wage, String emp_type, String emp_password,float emp_salary) {
        this.emp_id = -1;
        this.br_id = br_id;
        this.emp_name = emp_name;
        this.emp_email = emp_email;
        this.emp_phone_number = emp_phone_number;
        this.emp_address = emp_address;
        this.emp_position = emp_position;
        this.emp_wage = emp_wage;
        this.emp_type = emp_type;
        this.emp_password = emp_password;
        this.emp_salary = emp_salary;
    }

    public Employee() {
        this.emp_id = -1;
        this.br_id = 0;
        this.emp_name = "";
        this.emp_email = "";
        this.emp_phone_number = "";
        this.emp_address = "";
        this.emp_position = "";
        this.emp_wage = 0.0f;
        this.emp_type = "";
        this.emp_password = "";
        this.emp_salary = 0.0f;
    }

    public int getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(int emp_id) {
        this.emp_id = emp_id;
    }

    public int getBr_id() {
        return br_id;
    }

    public void setBr_id(int br_id) {
        this.br_id = br_id;
    }

    public String getEmp_password() {
        return emp_password;
    }

    public void setEmp_password(String emp_password) {
        this.emp_password = emp_password;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }

    public String getEmp_email() {
        return emp_email;
    }

    public void setEmp_email(String emp_email) {
        this.emp_email = emp_email;
    }

    public String getEmp_phone_number() {
        return emp_phone_number;
    }

    public void setEmp_phone_number(String emp_phone_number) {
        this.emp_phone_number = emp_phone_number;
    }

    public String getEmp_address() {
        return emp_address;
    }

    public void setEmp_address(String emp_address) {
        this.emp_address = emp_address;
    }

    public String getEmp_position() {
        return emp_position;
    }

    public void setEmp_position(String emp_position) {
        this.emp_position = emp_position;
    }

    public float getEmp_wage() {
        return emp_wage;
    }

    public void setEmp_wage(float emp_wage) {
        this.emp_wage = emp_wage;
    }

    public String getEmp_type() {
        return emp_type;
    }

    public void setEmp_type(String emp_type) {
        this.emp_type = emp_type;
    }

    public float getEmp_salary() {
        return emp_salary;
    }

    public void setEmp_salary(float emp_salary) {
        this.emp_salary = emp_salary;
    }

    @Override
    public String toString() {
        return "Employee{" + "emp_id=" + emp_id + ", br_id=" + br_id + ", emp_name=" + emp_name + ", emp_email=" + emp_email + ", emp_phone_number=" + emp_phone_number + ", emp_address=" + emp_address + ", emp_position=" + emp_position + ", emp_wage=" + emp_wage + ", emp_type=" + emp_type + ", emp_password=" + emp_password + ", emp_salary=" + emp_salary + '}';
    }

    public static Employee fromRS(ResultSet rs) {
        Employee emp = new Employee();
        try {
            emp.setEmp_id(rs.getInt("EMP_ID"));
            emp.setBr_id(rs.getInt("BR_ID"));
            emp.setEmp_name(rs.getString("EMP_NAME"));
            emp.setEmp_email(rs.getString("EMP_EMAIL"));
            emp.setEmp_phone_number(rs.getString("EMP_PHONE_NUMBER"));
            emp.setEmp_address(rs.getString("EMP_ADDRESS"));
            emp.setEmp_position(rs.getString("EMP_POSITION"));
            emp.setEmp_wage(rs.getFloat("EMP_WAGE"));
            emp.setEmp_type(rs.getString("EMP_TYPE"));
            emp.setEmp_password(rs.getString("EMP_PASSWORD"));
            emp.setEmp_salary(rs.getFloat("EMP_SALARY"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return emp; // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
