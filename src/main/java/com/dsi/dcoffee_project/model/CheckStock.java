/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Surap
 */
public class CheckStock {
    private int id;
    private int EmployeeId;
    private int BranchId;
    private String Date;
    private String Time;

    public CheckStock(int id, int EmployeeId, int BranchId, String Date, String Time) {
        this.id = id;
        this.EmployeeId = EmployeeId;
        this.BranchId = BranchId;
        this.Date = Date;
        this.Time = Time;
    }
    
    public CheckStock( int EmployeeId, int BranchId, String Date, String Time) {
        this.id = -1;
        this.EmployeeId = EmployeeId;
        this.BranchId = BranchId;
        this.Date = Date;
        this.Time = Time;
    }
    
    public CheckStock() {
        this.id = -1;
        this.EmployeeId = -1;
        this.BranchId = -1;
        this.Date = "";
        this.Time = "";
    }

   
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(int EmployeeId) {
        this.EmployeeId = EmployeeId;
    }

    public int getBranchId() {
        return BranchId;
    }

    public void setBranchId(int BranchId) {
        this.BranchId = BranchId;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String Time) {
        this.Time = Time;
    }

    @Override
    public String toString() {
        return "CheckStock{" + "id=" + id + ", EmployeeId=" + EmployeeId + ", BranchId=" + BranchId + ", Date=" + Date + ", Time=" + Time + '}';
    }





    
    
    
    
    
    public static CheckStock fromRS(ResultSet rs) {
        CheckStock cs = new CheckStock();
        try {
            cs.setId(rs.getInt("CHK_ID"));
            cs.setEmployeeId(rs.getInt("EMP_ID"));
            cs.setBranchId(rs.getInt("BR_ID"));
            cs.setDate(rs.getString("CHK_DATE"));
            cs.setTime(rs.getString("CHK_TIME"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckStock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return cs;
    
    }}
