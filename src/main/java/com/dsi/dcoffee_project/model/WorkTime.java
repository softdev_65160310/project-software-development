package com.dsi.dcoffee_project.model;

import com.dsi.dcoffee_project.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WorkTime {
    private int id;
    private int employee;
    private String date;
    private String checkIn;
    private String checkOut;
    private int totalWorked;
    private String status;
    private int overTime;

    
    private int overtime;

    public WorkTime(int id, String date, int employee, String checkIn, String checkOut, int totalWorked, String status,int overTime) {
        this.id = id;
        this.date = date;
        this.employee = employee;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.totalWorked = totalWorked;
        this.status = status;
        this.overTime = overTime;
    }
    
    public WorkTime(String date, int employee, String checkIn, String checkOut, int totalWorked, String status,int overTime) {
        this.id = -1;
        this.date = date;
        this.employee = employee;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.totalWorked = totalWorked;
        this.status = status;
        this.overTime = overTime;
    }
    
    public WorkTime() {
        this.id = -1;
        this.date = "";
        this.employee = -1;
        this.checkIn = "";
        this.checkOut = "";
        this.totalWorked = 0;
        this.status = "";
        this.overtime = 0;
    }
    
    public int getOvertime() {
        return overtime;
    }

    public void setOvertime(int overtime) {
        this.overtime = overtime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getEmployee() {
        return employee;
    }

    public void setEmployee(int employee) {
        this.employee = employee;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public int getTotalWorked() {
        return totalWorked;
    }

    public void setTotalWorked(int totalWorked) {
        this.totalWorked = totalWorked;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "WorkTime{" + "id=" + id + ", employee=" + employee + ", date=" + date + ", checkIn=" + checkIn + ", checkOut=" + checkOut + ", totalWorked=" + totalWorked + ", status=" + status + ", overtime=" + overtime + '}';
    }

    

    
    
    public static WorkTime fromRS(ResultSet rs) {
        WorkTime workTime = new WorkTime();
        
        try {
            workTime.setId(rs.getInt("W_ID"));
            workTime.setEmployee(rs.getInt("EMP_ID"));
            workTime.setDate(rs.getString("W_DATE"));
            workTime.setCheckIn(rs.getString("W_IN_TIME"));
            workTime.setCheckOut(rs.getString("W_OUT_TIME"));
            workTime.setTotalWorked(rs.getInt("W_TOTAL_TIME"));
            workTime.setStatus(rs.getString("W_STATUS"));
            workTime.setOvertime(rs.getInt("W_OT"));
            

        } catch (SQLException ex) {
            Logger.getLogger(WorkTime.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return workTime;
    }
    
}
