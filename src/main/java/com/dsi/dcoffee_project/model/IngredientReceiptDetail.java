/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.model;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sirapob
 */
public class IngredientReceiptDetail {
    private int indrecdt_id;
    private int ind_id;
    private int igrt_id;
    private String indrecdt_mfd;
    private String indrecdt_exp;
    private int indrecdt_qty;

    public IngredientReceiptDetail(int indrecdt_id, int ind_id, int igrt_id, String indrecdt_mfd, String indrecdt_exd,int indrecdt_qty) {
        this.indrecdt_id = indrecdt_id;
        this.ind_id = ind_id;
        this.igrt_id = igrt_id;
        this.indrecdt_mfd = indrecdt_mfd;
        this.indrecdt_exp = indrecdt_exd;
        this.indrecdt_qty = indrecdt_qty;
    }

    public IngredientReceiptDetail(int ind_id, int igrt_id, String indrecdt_mfd, String indrecdt_exd,int indrecdt_qty) {
        this.indrecdt_id = -1;
        this.ind_id = ind_id;
        this.igrt_id = igrt_id;
        this.indrecdt_mfd = indrecdt_mfd;
        this.indrecdt_exp = indrecdt_exd;
        this.indrecdt_qty = indrecdt_qty;
    }

    public IngredientReceiptDetail() {
        this.indrecdt_id = -1;
        this.ind_id = -1;
        this.igrt_id = -1;
        this.indrecdt_mfd = "";
        this.indrecdt_exp = "";
        this.indrecdt_qty = -1;
    }

    public int getIndrecdt_qty() {
        return indrecdt_qty;
    }

    public void setIndrecdt_qty(int indrecdt_qty) {
        this.indrecdt_qty = indrecdt_qty;
    }

    public int getIndrecdt_id() {
        return indrecdt_id;
    }

    public void setIndrecdt_id(int indrecdt_id) {
        this.indrecdt_id = indrecdt_id;
    }

    public int getInd_id() {
        return ind_id;
    }

    public void setInd_id(int ind_id) {
        this.ind_id = ind_id;
    }

    public int getIgrt_id() {
        return igrt_id;
    }

    public void setIgrt_id(int igrt_id) {
        this.igrt_id = igrt_id;
    }

    public String getIndrecdt_mfd() {
        return indrecdt_mfd;
    }

    public void setIndrecdt_mfd(String indrecdt_mfd) {
        this.indrecdt_mfd = indrecdt_mfd;
    }

    public String getIndrecdt_exp() {
        return indrecdt_exp;
    }

    public void setIndrecdt_exd(String indrecdt_exd) {
        this.indrecdt_exp = indrecdt_exd;
    }

    @Override
    public String toString() {
        return "IngredientReceiptDetail{" + "indrecdt_id=" + indrecdt_id + ", ind_id=" + ind_id + ", igrt_id=" + igrt_id + ", indrecdt_mfd=" + indrecdt_mfd + ", indrecdt_exd=" + indrecdt_exp + ", indrecdt_qty=" + indrecdt_qty + '}';
    }

    
    
    public static IngredientReceiptDetail fromRS(ResultSet rs) {
        IngredientReceiptDetail indrecdt = new IngredientReceiptDetail();
        try {
            indrecdt.setIndrecdt_id(rs.getInt("INDRECDT_ID"));
            indrecdt.setInd_id(rs.getInt("IND_ID"));
            indrecdt.setIgrt_id(rs.getInt("IGRT_ID"));
            indrecdt.setIndrecdt_mfd(rs.getString("INDRECDT_MFD"));
            indrecdt.setIndrecdt_exd(rs.getString("INDRECDT_EXD"));
            indrecdt.setIndrecdt_qty(rs.getInt("INDRECDT_QTY"));
        } catch (SQLException ex) {
            Logger.getLogger(IngredientReceiptDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return indrecdt; // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
