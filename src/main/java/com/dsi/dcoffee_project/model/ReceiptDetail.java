/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.model;

import com.dsi.dcoffee_project.dao.ProductDao;
import com.dsi.dcoffee_project.dao.ReceiptDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BoM
 */
public class ReceiptDetail {

    private int Id;
    private int ReceiptId;
    private int ProductId;
    private Product product;
    private int Qty;
    private float Price;
    private float Discount;
    private float TotalPrice;

    public ReceiptDetail(int Id, int ReceiptId, int ProductId,  Product product, int Qty, float Price, float Discount, float TotalPrice) {
        this.Id = Id;
        this.ReceiptId = ReceiptId;
        this.ProductId = ProductId;
        this.product = product;
        this.Qty = Qty;
        this.Price = Price;
        this.Discount = Discount;
        this.TotalPrice = TotalPrice;
    }

    public ReceiptDetail(int ReceiptId, int ProductId,  Product product, int Qty, float Price, float Discount, float TotalPrice) {
        this.Id = -1;
        this.ReceiptId = ReceiptId;
        this.ProductId = ProductId;
        this.product = product;
        this.Qty = Qty;
        this.Price = Price;
        this.Discount = Discount;
        this.TotalPrice = TotalPrice;
    }

    public ReceiptDetail() {
        this.Id = -1;
        this.ReceiptId = 0;
        this.ProductId = 0;
        this.product = null;
        this.Qty = 0;
        this.Price = 0;
        this.Discount = 0;
        this.TotalPrice = 0;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getReceiptId() {
        return ReceiptId;
    }

    public void setReceiptId(int ReceiptId) {
        this.ReceiptId = ReceiptId;
    }

    public int getProductId() {
        return ProductId;
    }

    public void setProductId(int ProductId) {
        this.ProductId = ProductId;
    }


    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int Qty) {
        this.Qty = Qty;
    }

    public float getPrice() {
        return Price;
    }

    public void setPrice(float Price) {
        this.Price = Price;
    }

    public float getDiscount() {
        return Discount;
    }

    public void setDiscount(float Discount) {
        this.Discount = Discount;
    }

    public float getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(float TotalPrice) {
        this.TotalPrice = TotalPrice;
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "Id=" + Id + ", ReceiptId=" + ReceiptId + ", ProductId=" + ProductId + ", product=" + product + ", Qty=" + Qty + ", Price=" + Price + ", Discount=" + Discount + ", TotalPrice=" + TotalPrice + '}';
    }



    public static ReceiptDetail fromRS(ResultSet rs) {
        ReceiptDetail receiptDetail = new ReceiptDetail();

        try {

            receiptDetail.setId(rs.getInt("RD_ID"));
            receiptDetail.setReceiptId(rs.getInt("R_ID"));
            receiptDetail.setProductId(rs.getInt("PD_ID"));
            receiptDetail.setQty(rs.getInt("PD_QUANTITY"));
            receiptDetail.setPrice(rs.getInt("RD_PRICE"));
            receiptDetail.setDiscount(rs.getFloat("RD_DISCOUNT"));
            receiptDetail.setTotalPrice(rs.getFloat("RD_TOTALPRICE"));
        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receiptDetail;
    }

//        InvoiceDetail invD = new InvoiceDetail();
//        ProductDao productDao = new ProductDao();
//        InvoiceDao invoiceDao = new InvoiceDao();
//        int product_id;
//        int invoice_id;
//
//        try {
//            // get only id for set object by check that id
//            product_id = rs.getInt("product_id");
//            invoice_id = rs.getInt("invoice_id");
//            // set object
//            invD.setId(rs.getInt("invoiceDetail_id"));
//            invD.setUnit(rs.getInt("invoiceDetail_unit"));
//            invD.setUnitprice(rs.getFloat("invoiceDetail_unitPrice"));
//            invD.setDiscount(rs.getFloat("invoiceDetail_discount"));
//            invD.setNetPrice(rs.getFloat("invoiceDetail_netPrice"));
//            invD.setProduct(productDao.get(product_id));
//            invD.setInvoice(invoiceDao.get(invoice_id));
//        } catch (SQLException ex) {
//            Logger.getLogger(Invoice.class.getName()).log(Level.SEVERE, null, ex);
//            return null;
//        }
//        return invD;
}
