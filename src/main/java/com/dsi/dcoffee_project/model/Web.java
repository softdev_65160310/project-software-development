/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class Web {
    private int Id;
    private int branchId;
    private int EmpId;
    private String Date;
    private int WaterUnitRead;
    private int ElcUnitRead;
    private int WaterUnitPerM;
    private int ElcUnitPerM;
    private int WaterPerUnit;
    private int ElcPerUnit;
    private int Total_Water;
    private int Total_Electric;
    private String Status;

    public Web(int Id, int branchId, int EmpId, String Date, int WaterUnitRead, int ElcUnitRead, int WaterUnitPerM, int ElcUnitPerM, int WaterPerUnit, int ElcPerUnit, int Total_Water, int Total_Electric, String Status) {
        this.Id = Id;
        this.branchId = branchId;
        this.EmpId = EmpId;
        this.Date = Date;
        this.WaterUnitRead = WaterUnitRead;
        this.ElcUnitRead = ElcUnitRead;
        this.WaterUnitPerM = WaterUnitPerM;
        this.ElcUnitPerM = ElcUnitPerM;
        this.WaterPerUnit = WaterPerUnit;
        this.ElcPerUnit = ElcPerUnit;
        this.Total_Water = Total_Water;
        this.Total_Electric = Total_Electric;
        this.Status = Status;
    }
    
    public Web(int branchId, int EmpId, String Date, int WaterUnitRead, int ElcUnitRead, int WaterUnitPerM, int ElcUnitPerM, int WaterPerUnit, int ElcPerUnit, int Total_Water, int Total_Electric, String Status) {
        this.Id = -1;
        this.branchId = branchId;
        this.EmpId = EmpId;
        this.Date = Date;
        this.WaterUnitRead = WaterUnitRead;
        this.ElcUnitRead = ElcUnitRead;
        this.WaterUnitPerM = WaterUnitPerM;
        this.ElcUnitPerM = ElcUnitPerM;
        this.WaterPerUnit = WaterPerUnit;
        this.ElcPerUnit = ElcPerUnit;
        this.Total_Water = Total_Water;
        this.Total_Electric = Total_Electric;
        this.Status = Status;
    }
    
    public Web() {
        this.Id = -1;
        this.branchId = 0;
        this.EmpId = 0;
        this.Date = "";
        this.WaterUnitRead = 0;
        this.ElcUnitRead = 0;
        this.WaterUnitPerM = 0;
        this.ElcUnitPerM = 0;
        this.WaterPerUnit = 0;
        this.ElcPerUnit = 0;
        this.Total_Water = 0;
        this.Total_Electric = 0;
        this.Status = "";
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getEmpId() {
        return EmpId;
    }

    public void setEmpId(int EmpId) {
        this.EmpId = EmpId;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public int getWaterUnitRead() {
        return WaterUnitRead;
    }

    public void setWaterUnitRead(int WaterUnitRead) {
        this.WaterUnitRead = WaterUnitRead;
    }

    public int getElcUnitRead() {
        return ElcUnitRead;
    }

    public void setElcUnitRead(int ElcUnitRead) {
        this.ElcUnitRead = ElcUnitRead;
    }

    public int getWaterUnitPerM() {
        return WaterUnitPerM;
    }

    public void setWaterUnitPerM(int WaterUnitPerM) {
        this.WaterUnitPerM = WaterUnitPerM;
    }

    public int getElcUnitPerM() {
        return ElcUnitPerM;
    }

    public void setElcUnitPerM(int ElcUnitPerM) {
        this.ElcUnitPerM = ElcUnitPerM;
    }

    public int getWaterPerUnit() {
        return WaterPerUnit;
    }

    public void setWaterPerUnit(int WaterPerUnit) {
        this.WaterPerUnit = WaterPerUnit;
    }

    public int getElcPerUnit() {
        return ElcPerUnit;
    }

    public void setElcPerUnit(int ElcPerUnit) {
        this.ElcPerUnit = ElcPerUnit;
    }

    public int getTotal_Water() {
        return Total_Water;
    }

    public void setTotal_Water(int Total_Water) {
        this.Total_Water = Total_Water;
    }

    public int getTotal_Electric() {
        return Total_Electric;
    }

    public void setTotal_Electric(int Total_Electric) {
        this.Total_Electric = Total_Electric;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    @Override
    public String toString() {
        return "Web{" + "Id=" + Id + ", branchId=" + branchId + ", EmpId=" + EmpId + ", Date=" + Date + ", WaterUnitRead=" + WaterUnitRead + ", ElcUnitRead=" + ElcUnitRead + ", WaterUnitPerM=" + WaterUnitPerM + ", ElcUnitPerM=" + ElcUnitPerM + ", WaterPerUnit=" + WaterPerUnit + ", ElcPerUnit=" + ElcPerUnit + ", Total_Water=" + Total_Water + ", Total_Electric=" + Total_Electric + ", Status=" + Status + '}';
    }
    

    public static Web fromRS(ResultSet rs) {
        Web web = new Web();
        try {
            web.setId(rs.getInt("WEB_ID"));
            web.setBranchId(rs.getInt("BR_ID"));
            web.setEmpId(rs.getInt("EMP_ID"));
            web.setDate(rs.getString("WEB_DATE"));
            web.setWaterUnitRead(rs.getInt("WEB_WATER_UNIT_READ"));
            
            web.setElcUnitRead(rs.getInt("WEB_ELC_UNIT_READ"));
            web.setWaterUnitPerM(rs.getInt("WEB_WATER_UNIT_PERMONTH"));
            web.setElcUnitPerM(rs.getInt("WEB_ELC_UNIT_PERMONTH"));
            web.setWaterPerUnit(rs.getInt("WEB_WATER_PERUNIT"));
            web.setElcPerUnit(rs.getInt("WEB_ELECTRIC_PERUNIT"));
            web.setTotal_Water(rs.getInt("WEB_TOTAL_WATER"));
            web.setTotal_Electric(rs.getInt("WEB_TOTAL_ELECTRIC"));
            web.setStatus(rs.getString("WEB_STATUS"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Web.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return web;
    }
    
    
    
    
}
