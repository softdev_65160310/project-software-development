/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;


import com.dsi.dcoffee_project.dao.PromotionBranchDao;

import com.dsi.dcoffee_project.model.PromotionBranch;
import java.util.List;

/**
 *
 * @author Tanakorn
 */
public class PromotionBranchService {
    
    public PromotionBranch getById(int id) {
        PromotionBranchDao promotionBranchDao = new PromotionBranchDao();
        return promotionBranchDao.get(id);
    }
    
    public List<PromotionBranch> getPromotionBranchs(){
        PromotionBranchDao promotionBranchDao = new PromotionBranchDao();
        return promotionBranchDao.getAll("PTB_ID asc");
    }
    
    public PromotionBranch addNew(PromotionBranch editedPromotionBranch) {
        PromotionBranchDao promotionBranchDao = new PromotionBranchDao();
        return promotionBranchDao.save(editedPromotionBranch);
    }

    public PromotionBranch update(PromotionBranch editedPromotionBranch) {
        PromotionBranchDao promotionBranchDao = new PromotionBranchDao();
        return promotionBranchDao.update(editedPromotionBranch);
    }

    public int delete(PromotionBranch editedPromotionBranch) {
        PromotionBranchDao promotionBranchDao = new PromotionBranchDao();
        return promotionBranchDao.delete(editedPromotionBranch);
    }
    
}
