/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;

import com.dsi.dcoffee_project.dao.WebDao;
import com.dsi.dcoffee_project.model.Web;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class WebService {
    
    public Web getById(int id){
        WebDao WebDao = new WebDao();
        return WebDao.get(id);
    }

    public List<Web> getWebAll() {
        WebDao WebDao = new WebDao();
        return WebDao.getAll("WEB_ID asc");
    }

    public Web addNew(Web editedWeb) {
        WebDao WebDao = new WebDao();
        return WebDao.save(editedWeb);
    }

    public Web update(Web editedWeb) {
        WebDao WebDao = new WebDao();
        return WebDao.update(editedWeb);
    }

    public int delete(Web editedWeb) {
        WebDao WebDao = new WebDao();
        return WebDao.delete(editedWeb);
    }
}
