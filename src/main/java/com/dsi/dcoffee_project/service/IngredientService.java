/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;

import com.dsi.dcoffee_project.dao.IngredientDao;
import com.dsi.dcoffee_project.dao.IngredientDao;
import com.dsi.dcoffee_project.model.Ingredient;
import com.dsi.dcoffee_project.model.Ingredient;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author BAM
 */
public class IngredientService {
    public ArrayList<Ingredient> getAll() {
        IngredientDao ingredientDao = new IngredientDao();
        return (ArrayList<Ingredient>) ingredientDao.getAll();
    }
    
    public Ingredient getById(int id) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.get(id);
    }
    
    public Ingredient getByName(String name) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.getByName(name);
    }
    
    public List<Ingredient> getIngredients(){
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.getAll("IND_ID asc");
    }
    
    public Ingredient addNew(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.save(editedIngredient);
    }

    public Ingredient update(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.update(editedIngredient);
    }

    public int delete(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.delete(editedIngredient);
    }
    
}
