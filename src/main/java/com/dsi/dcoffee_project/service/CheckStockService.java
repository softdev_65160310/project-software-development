/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;

import com.dsi.dcoffee_project.dao.CheckStockDao;
import com.dsi.dcoffee_project.model.CheckStock;
import java.util.List;

/**
 *
 * @author Surap
 */
public class CheckStockService {
    
    public CheckStock getById(int id) {
        
        CheckStockDao productDao = new CheckStockDao();
        return productDao.get(id);
    }
    

    
    public List<CheckStock> getCheckStocks(){
        CheckStockDao checkStockDao = new CheckStockDao();
        return checkStockDao.getAll("CHK_ID asc");
    }

    public CheckStock addNew(CheckStock editedCheckStock) {
        CheckStockDao checkStockDao = new CheckStockDao();
        return checkStockDao.save(editedCheckStock);
    }

    public CheckStock update(CheckStock editedCheckStock) {
        CheckStockDao checkStockDao = new CheckStockDao();
        return checkStockDao.update(editedCheckStock);
    }

    public int delete(CheckStock editedCheckStock) {
        CheckStockDao checkStockDao = new CheckStockDao();
        return checkStockDao.delete(editedCheckStock);
    }

}
