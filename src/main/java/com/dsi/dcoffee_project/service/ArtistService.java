/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;

import com.dsi.dcoffee_project.dao.ArtistDao;
import com.dsi.dcoffee_project.model.ArtistReport;
import java.util.List;

/**
 *
 * @author Surap
 */
public class ArtistService {
    public List<ArtistReport> getTopTenArtistByTotalPrice(){
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByTotalPrice(5);
    }
    public List<ArtistReport> getTopTenArtistByTotalPrice(String begin, String end){
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByTotalPrice(begin, end, 5);
    }
}
