/*
 * Click nbfs://nbhost/SystemFileSystem/Tcustomerlates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Tcustomerlates/Classes/Class.java to edit this tcustomerlate
 */
package com.dsi.dcoffee_project.service;

import com.dsi.dcoffee_project.dao.CustomerDao;
import com.dsi.dcoffee_project.model.Customer;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class CustomerService {
    
    public Customer getByTel(String tel) {
        CustomerDao customerdao = new CustomerDao();
        Customer customer = customerdao.getByTel(tel); 
        return customer;
    }
    public Customer getById(int id) {  
        CustomerDao customerdao = new CustomerDao();
        return customerdao.get(id);
    }

    public List<Customer> getCustomer() {
        CustomerDao CustomerDao = new CustomerDao();
        return CustomerDao.getAll("CUS_ID asc");
    }

    public Customer addNew(Customer editedCustomer) {
        CustomerDao CustomerDao = new CustomerDao();
        return CustomerDao.save(editedCustomer);
    }

    public Customer update(Customer editedCustomer) {
        CustomerDao CustomerDao = new CustomerDao();
        return CustomerDao.update(editedCustomer);
    }

    public int delete(Customer editedCustomer) {
        CustomerDao CustomerDao = new CustomerDao();
        return CustomerDao.delete(editedCustomer);
    }

}
