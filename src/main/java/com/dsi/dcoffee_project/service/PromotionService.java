/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;


import com.dsi.dcoffee_project.dao.PromotionDao;

import com.dsi.dcoffee_project.model.Promotion;
import java.util.List;

/**
 *
 * @author BAM
 */
public class PromotionService {
    
    public Promotion getById(int id) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.get(id);
    }
    
    public List<Promotion> getPromotions(){
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.getAll("PT_ID asc");
    }
    
    public List<Promotion> getPromotionsByON(){
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.getByStatusOn("ON");
    }
    
    public Promotion addNew(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.save(editedPromotion);
    }

    public Promotion update(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.update(editedPromotion);
    }

    public int delete(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.delete(editedPromotion);
    }
    
}
