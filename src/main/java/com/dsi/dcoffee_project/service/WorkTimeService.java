package com.dsi.dcoffee_project.service;

import com.dsi.dcoffee_project.dao.WorkTimeDao;
import com.dsi.dcoffee_project.model.WorkTime;
import com.dsi.dcoffee_project.model.Employee;
import java.util.ArrayList;
import java.util.List;

public class WorkTimeService {

    public ArrayList<WorkTime> getAll() {
        WorkTimeDao workTimeDao = new WorkTimeDao();
        return (ArrayList<WorkTime>) workTimeDao.getAll();
    }
    


    public List<WorkTime> getCheckInOutByOrder(String where, String order) {
        WorkTimeDao checkTimeDao = new WorkTimeDao();
        return checkTimeDao.getAll(where, order);
    }

    public List<WorkTime> getCheckInOutByOrderDESC() {
        WorkTimeDao checkTimeDao = new WorkTimeDao();
        return checkTimeDao.getAll("W_ID", "DESC");
    }

    public void addNew(WorkTime time) {
        WorkTimeDao workTimeDao = new WorkTimeDao();
        workTimeDao.insert(time);
    }

    public void update(WorkTime time) {
        WorkTimeDao workTimeDao = new WorkTimeDao();
        workTimeDao.update(time);
    }

    public void updateCheckOut(WorkTime time) {
        WorkTimeDao workTimeDao = new WorkTimeDao();
        workTimeDao.updateCheckOut(time);
    }

    public void delete(WorkTime time) {
        WorkTimeDao workTimeDao = new WorkTimeDao();
        workTimeDao.delete(time);
    }

    public WorkTime getForDelete(int id, String time) {
        WorkTimeDao workTimeDao = new WorkTimeDao();
        return workTimeDao.getForDeletet(id, time);
    }
    
    public WorkTime getByDateEmp(String date,int empId) {
        WorkTimeDao workTimeDao = new WorkTimeDao();
        return workTimeDao.getByDateEmp(date,empId);
    }
    
    public WorkTime checkOutById(int id){
        WorkTimeDao workTimeDao = new WorkTimeDao();
        return workTimeDao.checkOutById(id);
    }
}
