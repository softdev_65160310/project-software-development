/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;


import com.dsi.dcoffee_project.dao.IngredientReceiptDao;
import com.dsi.dcoffee_project.model.IngredientReceipt;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class IngredientReceiptService {
   public ArrayList<IngredientReceipt> getAll() {
        IngredientReceiptDao indrecDao = new IngredientReceiptDao();
        return (ArrayList<IngredientReceipt>) indrecDao.getAll();
    }
    public IngredientReceipt getById(int id) {
        IngredientReceiptDao indrecDao = new IngredientReceiptDao();
        return indrecDao.get(id);
    }
    public List<IngredientReceipt> getUsersByOrder(String where, String order) {
        IngredientReceiptDao indrecDao = new IngredientReceiptDao();
        return indrecDao.getAll(where, order);
    }
    public IngredientReceipt update(IngredientReceipt editedIngredientReceipt) {
        IngredientReceiptDao indrecDao = new IngredientReceiptDao();
        return indrecDao.update(editedIngredientReceipt);
    }
    public int delete(IngredientReceipt editedIngredientReceipt) {
        IngredientReceiptDao indrecDao = new IngredientReceiptDao();
        return indrecDao.delete(editedIngredientReceipt);
    }
    public IngredientReceipt Save(IngredientReceipt editedIngredientReceipt) {
        IngredientReceiptDao indrecDao = new IngredientReceiptDao();
        return indrecDao.save(editedIngredientReceipt);
    }
    public IngredientReceipt addNew(IngredientReceipt editedIngredientReceipt) {
        IngredientReceiptDao indrecdtDao = new IngredientReceiptDao();
        return indrecdtDao.save(editedIngredientReceipt);
    }
}
