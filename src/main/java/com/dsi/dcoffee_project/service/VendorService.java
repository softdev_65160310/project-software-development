/*
 * Click nbfs://nbhost/SystemFileSystem/Tvendorlates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Tvendorlates/Classes/Class.java to edit this tvendorlate
 */
package com.dsi.dcoffee_project.service;

import com.dsi.dcoffee_project.dao.VendorDao;
import com.dsi.dcoffee_project.model.Vendor;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class VendorService {
    
    public Vendor getByName(String name) {
        VendorDao vendordao = new VendorDao();
        Vendor vendor = vendordao.getByName(name); 
        return vendor;
    }
    
    public Vendor getById(int id){
        VendorDao vendorDao = new VendorDao();
        return vendorDao.get(id);
    }

    public List<Vendor> getVendor() {
        VendorDao VendorDao = new VendorDao();
        return VendorDao.getAll("VD_ID asc");
    }

    public Vendor addNew(Vendor editedVendor) {
        VendorDao VendorDao = new VendorDao();
        return VendorDao.save(editedVendor);
    }

    public Vendor update(Vendor editedVendor) {
        VendorDao VendorDao = new VendorDao();
        return VendorDao.update(editedVendor);
    }

    public int delete(Vendor editedVendor) {
        VendorDao VendorDao = new VendorDao();
        return VendorDao.delete(editedVendor);
    }

}
