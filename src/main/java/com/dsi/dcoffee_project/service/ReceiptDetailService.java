/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;

import com.dsi.dcoffee_project.dao.ReceiptDao;
import com.dsi.dcoffee_project.dao.ReceiptDetailDao;
import com.dsi.dcoffee_project.model.Receipt;
import com.dsi.dcoffee_project.model.ReceiptDetail;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author BoM
 */
public class ReceiptDetailService {
    public List<ReceiptDetail> getAll(){
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return (ArrayList<ReceiptDetail>) receiptDetailDao.getAll();
    }
//    public ReceiptDetail getOneRD(int order){
//        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
//        return receiptDetailDao.get(order);
//    }
//    
    public List<ReceiptDetail> getUsersByOrder(String where, String order) {
        ReceiptDetailDao invoiceDetailDao = new ReceiptDetailDao();
        return invoiceDetailDao.getAll(where, order);
    }
    
    public List<ReceiptDetail> getInvoiceDetailByOrderDESC() {
        ReceiptDetailDao invoiceDetailDao = new ReceiptDetailDao();
        return invoiceDetailDao.getAll("RD_ID", "DESC");
    }

    public ReceiptDetail addNew(ReceiptDetail editedReceiptDetail) {
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return receiptDetailDao.save(editedReceiptDetail);
    }

    public ReceiptDetail update(ReceiptDetail editedReceiptDetail) {
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return receiptDetailDao.update(editedReceiptDetail);
    }

    public int delete(ReceiptDetail editedReceiptDetail) {
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return receiptDetailDao.delete(editedReceiptDetail);
    }
}
