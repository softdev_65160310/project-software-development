/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test_service;

import com.dsi.dcoffee_project.model.Vendor;
import com.dsi.dcoffee_project.service.VendorService;

/**
 *
 * @author User
 */
public class TestVendorService {
    public static void main(String[] args) {
        VendorService vd = new VendorService();
        for(Vendor vendor: vd.getVendor()){
            System.out.println(vendor);
        }
        System.out.println(vd.getByName("CoffShop"));
        Vendor vender1 = new Vendor("Las Vegas", "Las Vegas", "0999818181", "LasVegas@gmail.com");
        vd.addNew(vender1);
        for(Vendor vendor: vd.getVendor()){
            System.out.println(vendor);
        }
        
        Vendor delven = vd.getByName("Las Vegas");
        delven.setName("Las vegas");
        vd.update(delven);
        System.out.println("Update");
        for(Vendor vendor: vd.getVendor()){
            System.out.println(vendor);
        }
        
        System.out.println("Delete");
        vd.delete(delven);
        for(Vendor vendor: vd.getVendor()){
            System.out.println(vendor);
        }
    }
    
}
