/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test_service;

import com.dsi.dcoffee_project.model.Promotion;
import com.dsi.dcoffee_project.service.PromotionService;

/**
 *
 * @author aotto
 */
public class TestPromotionService {
    public static void main(String[] args) {
        PromotionService cs = new PromotionService();
        for(Promotion promotion:cs.getPromotions()) {
            System.out.println(promotion);
        }
    }
}
