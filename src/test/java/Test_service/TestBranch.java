/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test_service;

import com.dsi.dcoffee_project.model.Branch;
import com.dsi.dcoffee_project.service.BranchService;

/**
 *
 * @author jeep
 */
public class TestBranch {

    public static void main(String[] args) {
        BranchService br = new BranchService();

        Branch brAdd = new Branch("Chachoengsao", "603 On Nut Road,Ban Pho,Chachoengsao", "0655020205", "24140");
        br.addNew(brAdd);
        
        Branch brUpdate = br.getById(3);
        brUpdate.setId(3);
        br.update(brUpdate);
        
        Branch brDel = br.getById(3);
        br.delete(brDel);
        
        
        for (Branch branch : br.getBranchs()) {
            System.out.println(branch);
        }
        
    }
}
       


